import { useState } from 'react';
import './App.css';
import Button from './components/Button/Button';
import Modal from './containers/Modal/Modal';
import ModalSecond from './containers/Modal/ModalSecond';

function App() {
  const [modalActive, setModalActive] = useState(false)
  const [modalSecondActive, setSecondModalActive] = useState(false)
  return (
    <>
      <Button violetButton click={()=>setModalActive(true)}>Open first modal</Button>
      <Button violetFontButton click={() => setSecondModalActive(true)}>Open second modal</Button>
      <Modal active={modalActive} setActive={setModalActive} />
      <ModalSecond modalSecondActive={modalSecondActive} setSecondModalActive={setSecondModalActive} />
    </>
  );
}

export default App;

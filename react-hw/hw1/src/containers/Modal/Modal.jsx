import Button from "../../components/Button/Button";
import TitleText from "../../components/Text/Title";
import ParagraphText from "../../components/Text/Paragraph";
import ModalFooter from "./ModalFooter.jsx";
import './Modal.scss'
import cn from "classnames"
import Close from "../../components/Button/Close";
import ModalImage from "./ModalImage"


function Modal({ active, setActive }) {
    
    // const { header = '', closeButton, text, actions, } = props;
    return (
        <div className={active ? "modal active" : "modal"} onClick={()=> setActive(false)}>
            <div className="modal-content" onClick={e => e.stopPropagation()}>
                <Close className={active ? "modal active" : "modal"} click={() => setActive(false)} />
                <ModalImage src="https://www.lego.com/cdn/cs/set/assets/blt5ca8d7adce06a3a4/42122.jpg?fit=bounds&format=jpg&quality=80&width=1500&height=1500&dpr=1" alt="Lego car"/>
                <TitleText>Product Delete!</TitleText>
                <ParagraphText>By clicking the "Yes, Delete" button, PRODUCT NAME will be deleted.</ParagraphText>
                <ModalFooter textFirst="No, cancel" clickFirst={active} textSecondary="Yes, delete"/>
            </div>
        </div>
    )
}

export default Modal
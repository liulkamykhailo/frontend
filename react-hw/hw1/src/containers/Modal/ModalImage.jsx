

function ModalImage(props) {
    const { alt, src } = props
    return (
        <img style={{width: 270, height: 150}} src={src} alt={alt} />
    )
}

export default ModalImage
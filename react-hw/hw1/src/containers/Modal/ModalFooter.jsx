import Button from "../../components/Button/Button"

const ModalFooter = ({textFirst, textSecondary, clickFirst, clickSecondary}) => {
    return (
        <div className="button-wrapper">
            {textFirst && <Button violetButton click={clickFirst}>{ textFirst }</Button>}
            {textSecondary && <Button violetFontButton click={clickSecondary}>{ textSecondary }</Button>}
        </div>
    )
}
export default ModalFooter
import Button from "../../components/Button/Button";
import TitleText from "../../components/Text/Title";
import ParagraphText from "../../components/Text/Paragraph";
import ModalFooter from "./ModalFooter.jsx";
import './Modal.scss'
import cn from "classnames"
import Close from "../../components/Button/Close";
import ModalImage from "./ModalImage"


function ModalSecond({ modalSecondActive, setSecondModalActive }) {
    
    // const { header = '', closeButton, text, actions, } = props;
    return (
        <div className={modalSecondActive ? "modal active" : "modal"} onClick={()=> setSecondModalActive(false)}>
            <div className="modal-content" onClick={e => e.stopPropagation()}>
                <Close className={modalSecondActive ? "modal active" : "modal"} click={() => setSecondModalActive(false)} />
                <TitleText>Add Product "NAME"</TitleText>
                <ParagraphText>Description for product</ParagraphText>
                <ModalFooter textFirst="Add to favorite" clickFirst={modalSecondActive}/>
            </div>
        </div>
    )
}

export default ModalSecond
import './Close.scss'

function Close({click = () => { }}) {
    return (
        <button onClick={click} className="close"></button>
    )
}

export default Close
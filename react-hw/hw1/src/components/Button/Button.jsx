import cn from "classnames"
import './Button.scss'

function Button(props) {
    const {type = "button", classNames='', violetButton, violetFontButton, children, click=() => {},} = props
    return (
        <button onClick={click} className={cn("button", classNames, { "_violet" : violetButton }, { "_violet-font" : violetFontButton })} type={type}>{children}</button>
    )
}

export default Button
import './Text.scss'

function ParagraphText({ children }) {
    return (
        <p className="paragraph">{ children }</p>
    )
}
export default ParagraphText
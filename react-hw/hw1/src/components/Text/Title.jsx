import classNames from "classnames";
import './Text.scss'

function TitleText({children}) {
    return (
        <h2 className='title'>{ children }</h2>
    )
}
export default TitleText
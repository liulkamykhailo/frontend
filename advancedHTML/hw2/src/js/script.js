let headerNavActive = document.querySelectorAll('.header-item');

headerNavActive.forEach(i => {
    i.addEventListener('click', function () {
        headerNavActive.forEach(j => j.classList.remove('active-header'));
        this.classList.add('active-header');
    });
})


let headerBurgerMenuImg = document.querySelector('.header-burger-menu-img');
let headerBlockUl = document.querySelector('.header-block-ul');
headerBurgerMenuImg.addEventListener('click', function () {
    let src = headerBurgerMenuImg.getAttribute('src');
    let newSrc = src === './img/menu-button.png' ? './img/menu-button-2.png' : './img/menu-button.png';
    headerBurgerMenuImg.setAttribute('src', newSrc);

    headerBlockUl.classList.toggle('header-active-block')
})


// const viewportWidth = window.innerWidth;
// if (viewportWidth === 321) {
//     headerBlockUl.classList.remove('header-active-block')
// }
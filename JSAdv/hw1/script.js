'use strict'
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name
    }
    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age
    }
    set age(value) {
        this._age = +value;
    }

    get salary() {
        return this._salary
    }
    set salary(value) {
        this._salary = +value;
    }
}

let user = new Employee('User One', 25, 10);
console.log(user);
console.log('employee user -->', user.name);
console.log('employee user -->', user.age);
console.log('employee user -->', user.salary);

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
    set salary(value) {
        this._salary = +value;
    }

    get lang() {
        return this._lang;
    }
    set lang(value) {
        this._lang = value;
    }
}

const admin = new Programmer('Ivan', 20, 1000, ['english', 'ukrainian', 'france']);
console.log(admin);
console.log('programmer admin -->', admin.name);
console.log('programmer admin -->', admin.age);
console.log('programmer admin -->', admin.salary);
console.log('programmer admin -->', admin.lang);

const staff = new Programmer('Volodymyr', 23, 100, ['ruby', 'java']);
console.log(staff);
console.log('programmer staff -->', staff.name);
console.log('programmer staff -->', staff.age);
console.log('programmer staff -->', staff.salary);
console.log('programmer staff -->', staff.lang);


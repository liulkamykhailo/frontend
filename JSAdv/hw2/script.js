'use strict'
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.createElement('div');
root.id = 'root';
document.body.append(root);


function listBuilder(arr, parent = document.body) {
    let ul = document.createElement('ul')
    parent.append(ul)

    arr.forEach(element => {
        let author = element.author
        let name = element.name
        let price = element.price

        try {
            if (!author) {
                throw new Error(`No property: author `)
            } else if (!name) {
                throw new Error(`No property: name`)
            } else if (!price) {
                throw new Error(`No property: price`)
            }
            let ul = document.createElement('ul')
            parent.append(ul)
            let liAuthor = document.createElement('li')
            let liName = document.createElement('li')
            let liPrice = document.createElement('li')
            liAuthor.textContent = `Author: ${element.author}`;
            liName.textContent = `Name: ${element.name}`;
            liPrice.textContent = `Price: ${element.price}`;
            ul.append(liAuthor);
            ul.append(liName);
            ul.append(liPrice);
        } catch (err) {
            console.error(err.message)
        }

    });
}
listBuilder(books, root);

function sendRequest(url) {
    return fetch(url)
};

let url = 'https://ajax.test-danit.com/api/swapi/films';
let loading = document.querySelector('.typewriter');
let body = document.body

function showMovies(url) {
    sendRequest(url)
        .then(responce => responce.json())
        .then(data => {
            console.log(data);
            data.sort(function (a, b) {
                return a.id - b.id;
            })
            data.forEach(({ name, characters, episodeId, openingCrawl }) => {
                let div = document.createElement('div')
                div.classList = "movie";
                let h3 = document.createElement('h3')
                let ul = document.createElement('ul')
                let p = document.createElement('p')
                body.append(div);
                div.append(h3, ul, p)
                ul.innerHTML = `<b>Actors:</b>`
                h3.innerHTML = `${episodeId} - ${name}`
                p.innerHTML = `${openingCrawl}`


                characters.forEach((actor) => {
                    sendRequest(actor)
                        .then(responce => responce.json())
                        .then(data => {
                            let li = document.createElement('li')
                            li.innerText = `${data.name}`
                            ul.append(li)
                        })
                })
            });
        })
        .finally(() => {
            loading.remove()
        })
}
showMovies(url)
console.log(sendRequest('https://ajax.test-danit.com/api/swapi/people/63'));


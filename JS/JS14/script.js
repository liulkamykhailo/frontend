let changeThemeButton = document.querySelector('#changeThemeButton');
changeThemeButton.addEventListener('click', () => {
    if (localStorage.getItem('theme') === 'dark-theme') {
        localStorage.removeItem('theme')
    } else {
        localStorage.setItem('theme', 'dark-theme')
    }
    changeTheme()
})
function changeTheme() {
    if (localStorage.getItem('theme') === 'dark-theme') {
        document.querySelector('body').classList.add('dark-theme')
        document.querySelector('header').classList.replace('main-header', 'dark-theme')
    } else {
        document.querySelector('body').classList.remove('dark-theme')
        document.querySelector('header').classList.replace('dark-theme', 'main-header')

    }
}
changeTheme()
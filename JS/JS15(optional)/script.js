
let number = +prompt("Enter your number");
while (number === null || number === "" || isNaN(number)) {
    number = +prompt("Enter your number", number);
}
function factorial(num) {
    // let result = num;
    // for (let i = 1; i < num; i++) {
    //     result *= i;
    // }
    // return result

    if (num === 1) {
        return 1;
    } else {
        return factorial(num - 1) * num;
    }
}
console.log(factorial(number));
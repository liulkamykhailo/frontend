"use strict"

//1 DOM - це по суті весь HTML документ і все що в ньому є пробіли, коментарі, теги
//2 innerText додає текстову частину така як є в лапках, заміняє попередній,  innerHTML так само, але вже зчитуються HTML теги
//3 За допомогою методів getElementBy* та querySelector*


let allP = document.querySelectorAll("p");
for (const i of allP) {
    i.style.backgroundColor = "#ff0000"
}

let optionsList = document.getElementById("optionsList");
console.log(optionsList);
let parentElement = optionsList.parentElement;
console.log(parentElement);
let childNode = parentElement.childNodes;
console.log(childNode);
for (const i of childNode) {
    console.log(`Name: ${i.nodeName} type: ${i.nodeType}`);
}

let testParagraph = document.getElementById('testParagraph');
console.log(testParagraph);
testParagraph.innerText = "This is a paragraph";

let mainHeader = document.querySelector(".main-header");
let mainHeaderElements = mainHeader.children;
for (const e of mainHeaderElements) {
    console.log(e);
    e.classList.add("nav-item")
}

let sectionTitle = document.querySelectorAll(".section-title");
console.log(sectionTitle);
for (const i of sectionTitle) {
    i.classList.remove('section-title')
}
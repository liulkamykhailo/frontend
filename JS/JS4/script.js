//1. Функції використовуються для того аби виконувати один й той самий код в різних місцях
//2. Параметри використовуються функцією для використання у тілі фунцції, по суті це локальні змінні які набувають заданого значення
//3. Коли виконання функції доходить до return то код зупиняється і повертає вказане значення


let numberOne = prompt("Number one");
while (numberOne === null || isNaN(Number(numberOne)) || numberOne === "") {
    numberOne = prompt('Number one again', numberOne);
}

let numberTwo = prompt("Number two");
while (numberTwo === null || isNaN(Number(numberTwo)) || numberTwo === "") {
    numberTwo = prompt('Number two again', numberTwo);
}

let operator = prompt("Operator: +, -, *, /.");

let result


function math(numberOne, numberTwo, operator) {
    
    switch (operator) {
    case "+":
        result = +numberOne + +numberTwo;
        break;
    case "-":
        result = numberOne - numberTwo;
        break;
    case "/":
        if ((+numberOne === 0 || +numberTwo === 0) && operator === "/") {
           return console.error("На нуль ділити не можна!");
        } else {
            result = numberOne / numberTwo;  
        };
        break;
    case "*":
        result = numberOne * numberTwo;
        break;
    }
    
    return result;
};
math(numberOne, numberTwo, operator)

console.log(result);



let tabsTitle = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelectorAll(".tabs-content-item");

tabsTitle.forEach(e => {
    e.addEventListener('click', function () {
        let tabs = e.getAttribute('data-tab');
        let currentTab = document.querySelector(tabs);

        tabsTitle.forEach(i => {
            i.classList.remove('active')
        });

        tabsContent.forEach(j => {
            j.classList.remove('active')
        });

        e.classList.add('active');
        currentTab.classList.add('active');
    });
})
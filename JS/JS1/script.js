"use strict";
// 1.Як можна оголосити змінну у Javascript?
// За допомогоою let або const

// 2.У чому різниця між функцією prompt та функцією confirm?
// prompt - можна ввести дані користувача, які повертають отримане значення string або null якщо закрити вікно
// confirm - тільки 2 кнопки так чи ні, повертає true or false

// 3.Що таке неявне перетворення типів? Наведіть один приклад.
// Це коли різні типи даних конвертуються автоматично, наприклад
// let i = "5" + 1; //буде 51
// let i = "5" * 1; //буде 5


// 1
let name = "Misha";
let admin = name;
console.log(admin);

// 2
let days = +prompt("Число від 1 до 10");
let secondsInOneDay = 86400;
let result = days * secondsInOneDay;
console.log(result);

// 3
let userAge = +prompt("Вкажіть ваш вік");
console.log(`Ваш вік: ${userAge}`);
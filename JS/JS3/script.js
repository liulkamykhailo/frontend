// 1. Цикли потрібні для того аби виконувати один й той самий код безліч разів доки умова істинна
// 2. for використовуємо коли потрібно виконати певну кількість разів
// while буде виконуватись доки умова циклу є істинною
// do-while виконає цикл мінімум 1 раз навіть якщо умова false
//3. Явне перетворення типів відбувається за допомогою команд Number() String() Boolean(). Неявне - відбувається автоматично.



let userNumber = prompt("Enter your number");
while (isNaN(Number(userNumber)) || userNumber === null || userNumber === "") {
    userNumber = prompt('Enter your number again:');
}


for (let i = 0; i <= userNumber; i++) {
    
    if (i % 5 == 0 || i == 0) {
        console.log(i);
    };
};
if (userNumber < 5) {
    console.log("Sorry no numbers");
};
//1.Метод - це функціїя всередині об'єкта, яка може працювати зі змінними цього об'єкта
//2.Об'єкт може мати будь який тип даних
//3.Об'єкти копіюються за посиланням, а от наприклад примітивні типи даних такі як рядки, числа копіюються за значенням.




function createNewUser() {
    const newUser = {};

    let userName = prompt("Enter your name").trim();
    while (!userName || !isNaN(Number(userName))) {
        userName = prompt("Enter your name correctly");
    }
    let userSurname = prompt("Enter your surname").trim();
        while (!userSurname || !isNaN(Number(userSurname))) {
        userSurname = prompt("Enter your surname correctly");
    }

    // newUser.firstName = userName;
    Object.defineProperty(newUser, 'firstName', {
        value : userName,
        writable: false,
        configurable: true,
        });
    // newUser.lastName = userSurname;
    Object.defineProperty(newUser, 'lastName', {
    value : userSurname,
        writable: false,
        configurable: true,
    });

    newUser.getLogin = function () {
        return (this.firstName[0] + this.lastName).toLowerCase();
    };


    newUser.setFirstName = function () {
            Object.defineProperty(newUser, 'firstName', {
                value: prompt("Enter your NEW name"),
                writable: false,
                configurable: true,
            });
    };
    newUser.setSurname = function () {
            Object.defineProperty(newUser, 'lastName', {
                value: prompt("Enter your NEW surname"),
                writable: false,
                configurable: true,
            });
    };

    return newUser;
}

let user1 = createNewUser();
console.log(user1);
console.log(user1.getLogin());

user1.firstName = 'test name';
console.log(user1);
user1.setFirstName();
console.log(user1);

user1.lastName = 'test surname';
console.log(user1);
user1.setSurname();
console.log(user1);

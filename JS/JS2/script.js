//1. string, number, boolean, undefined, null, object, symbol, bigInt
//2. == це не строге порівняння без приведення типів, === це строге порівняння з приведенням типів
//3. Оператор це символ який виконує певну дію над операндами (наприклад додавання або множення)




let userName = prompt('Your name:');
while (userName === null || userName === "" || !isNaN(+userName)) {
    userName = prompt('Your name:', userName);
}

let userAge = prompt('Your age:');
while (userAge === null || isNaN(Number(userAge)) || userAge === "") {
    userAge = prompt('Your age:', userAge);
}


if (userAge < 18) {
    alert("You are not allowed to visit this website.");
} else if (18 <= userAge && userAge <= 22) {
    let greetings = confirm("Are you sure you want to continue?");
    if (greetings === true) {
        alert(`Welcome, ${userName}`);
    } else {
        alert("You are not allowed to visit this website.");
    }
} else {
    alert(`Welcome, ${userName}`);
};
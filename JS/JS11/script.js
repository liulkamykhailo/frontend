let eye = document.querySelectorAll('.fas')
let button = document.querySelector(".btn")
let inputOne = document.getElementById('inputOne')
let inputSecond = document.getElementById('inputSecond')
let error = document.querySelector('.error')

eye.forEach(e => {
    let input = e.closest('.input-wrapper').querySelector('input')

    e.addEventListener('click', () => {
        e.classList.toggle('fa-eye-slash')
        e.classList.toggle('fa-eye')

        if (e.classList.contains('fa-eye-slash')) {
            input.setAttribute('type', 'text')
        } else {
            input.setAttribute('type', 'password')
        }

    })
})

button.addEventListener('click', () => {
    if (inputOne.value !== inputSecond.value || inputOne.value === '' || inputSecond.value === '') {
        error.style.opacity = 1
    } else if (inputOne.value === inputSecond.value) {
        alert('You are welcome!')
        error.style.opacity = 0;
    }
})
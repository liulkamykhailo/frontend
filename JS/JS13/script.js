//1. setTimeout запускає функцію один раз через заданий час, а setInterval запускає функцію багаторазово, починаючи череззаданий час, і виконуючи постійно через цей проміжок часу
//2. з 0 затримкою виконає його якнайшвидше, але після виконання поточного скрипту
//3. clearInterval використовують для зупинки виконання інтервалу, інакше він буде продовжувати працювати


let startButton = document.querySelector('#startButton')
let stopButton = document.querySelector('#stopButton')
let restoreButton = document.querySelector('#restoreButton')
let images = document.querySelectorAll('.image-to-show')
let interval
let i = 0

function carousel() {
    interval = setInterval(() => {
        images[i].classList.remove('currrent-img');
        console.log(i);
        i++
        if (i >= images.length) {
            i = 0
        }
        images[i].classList.add('currrent-img');
    }, 3000)
}


startButton.addEventListener('click', () => {
    stopButton.classList.remove('button')
    restoreButton.classList.remove('button')

    carousel();
})
stopButton.addEventListener('click', () => {
    clearInterval(interval)
})
restoreButton.addEventListener('click', () => {
    clearInterval(interval)
    images.forEach(image => {
        image.classList.remove('currrent-img');
    });
    stopButton.classList.add('button')
    restoreButton.classList.add('button')
    i = 0
    images[i].classList.add('currrent-img');
})
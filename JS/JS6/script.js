//Екранування потрібне аби використовувати спеціальний символ як звичайний, наприклад лапки
//Function declaration - за допомогою слова function та function expression - записується як звичайна змінна
//Це коли наприклад функції доступні навіть до їх фізичного оголошення, в будь якому місці коду. Наче пересуваються на початок коду в глобальну область видимості






function createNewUser() {
    const newUser = {};

    let userName = prompt("Enter your name").trim();
    while (!userName || !isNaN(Number(userName))) {
        userName = prompt("Enter your name correctly");
    }
    let userSurname = prompt("Enter your surname").trim();
        while (!userSurname || !isNaN(Number(userSurname))) {
        userSurname = prompt("Enter your surname correctly");
    }

    let userBirthday = prompt("Enter your birthday  (dd.mm.yyyy)");
    while (userBirthday.length !== 10 || userBirthday[2] !== "." || userBirthday[5] !== ".") {
        userBirthday = prompt("Enter your birthday correctly (dd.mm.yyyy)");
    }
    let  splitUserDate = userBirthday.split(".");

    let userDate = new Date(`${splitUserDate[1]} / ${splitUserDate[0]} / ${splitUserDate[2]}`);


    newUser.birthday = userDate;

    // newUser.firstName = userName;
    Object.defineProperty(newUser, 'firstName', {
        value : userName,
        writable: false,
        configurable: true,
        });
    // newUser.lastName = userSurname;
    Object.defineProperty(newUser, 'lastName', {
    value : userSurname,
        writable: false,
        configurable: true,
    });

    newUser.getLogin = function () {
        return (this.firstName[0] + this.lastName).toLowerCase();
    };

    newUser.getPassword = function () {
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
    };

    newUser.getAge = function () {
        let dateNow = new Date();
        let age = dateNow.getFullYear() - this.birthday.getFullYear();
        if (this.birthday.getMonth() >= dateNow.getMonth() && this.birthday.getDate() >= dateNow.getDate()) {
            age--;
        }
        return age;
    };

    newUser.setFirstName = function () {
            Object.defineProperty(newUser, 'firstName', {
                value: prompt("Enter your NEW name"),
                writable: false,
                configurable: true,
            });
    };
    newUser.setSurname = function () {
            Object.defineProperty(newUser, 'lastName', {
                value: prompt("Enter your NEW surname"),
                writable: false,
                configurable: true,
            });
    };

    return newUser;
}

let user1 = createNewUser();
console.log(user1);
console.log(user1.getLogin());
console.log(user1.getPassword());
console.log(user1.getAge());

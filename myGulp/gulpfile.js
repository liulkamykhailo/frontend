//основний модуль
import gulp from 'gulp';
//імпорт шляхів
import { path } from './gulp/config/path.js';
//імпорт загальних плагінів
import { plugins } from './gulp/config/plugins.js';

global.app = {
    path: path,
    gulp: gulp,
    plugins: plugins
}

//імпорт задач
import { copy } from './gulp/tasks/copy.js';
import { reset } from './gulp/tasks/reset.js';
import { html } from './gulp/tasks/html.js';
import { server } from './gulp/tasks/server.js';
import { scss } from './gulp/tasks/scss.js';


function watcher(params) {
    gulp.watch(path.watch.files, copy);
    gulp.watch(path.watch.html, html);
    gulp.watch(path.watch.scss, scss);
}

//основні задачі
const mainTasks = gulp.parallel(copy, html, scss)
//побудова сценарію виконання задач
const dev = gulp.series(reset, mainTasks, gulp.parallel(watcher, server))
//виконання сценарію за замовчуванням
gulp.task('default', dev);


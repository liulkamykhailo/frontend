import replace from "gulp-replace";//пошук і заміна
import plumber from 'gulp-plumber'; //обработка помилок
import notify from 'gulp-notify'; //повідомлення, підказки
import browserSync from 'browser-sync'; //Локальний сервер

//експортує об'єкт
export const plugins = {
    replace: replace,
    plumber: plumber,
    notify: notify,
    browserSync: browserSync,
}
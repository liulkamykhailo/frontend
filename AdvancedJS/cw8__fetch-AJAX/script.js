// const sendRequest = async (url, options) => {
//     const response = await fetch(url, { ...options });
//     const result = await response.json(); //отримати data в форматі json
//     return result;
// }
//Ця функція все рівно буде Promis



// Axios
// .get('https://ajax.test-danit.com/api/json/users')
// .then(response => {
//     console.log(response);
// });







// Exersise 1

/**
* https://ajax.test-danit.com/api-pages/jsonplaceholder.html
Напишите GET запрос по адресу
"https://ajax.test-danit.com/api/json/users,
который получает список пользователей в формате
JSON и выводит его на экран внутри с id="users"
HTML
  <div class="user-item">
        <label class="user-label">
        <input type="radio" name="user" id="id">
            <p>
                <span class="item-name">name</span>: name
            </p>
            <p>
                <span class="item-name">username</span>: username
            </p>
        <p>
                <span class="item-name">email</span>: email
            </p>
        </label>
  </div>
Используйте Promise & XMLHttpRequest.

*/


// function sendRequest(url) {
//     return new Promise((resolve, reject) => {
//         const request = new XMLHttpRequest()
//         request.open("GET", url)
//         request.responseType = 'json'
//         request.send()
//         request.onload = () => {
//             if (request.status < 300) {
//                 resolve(request.response)
//             } else {
//                 reject(request.statusText)
//             }
//         }
//     })
// }

// const API = "https://ajax.test-danit.com/api/json/users";
// const root = document.getElementById('users');


// function renderUser(element) {
//     sendRequest(API)
//         .then(response => {
//             response.forEach(({ name, id, username, email }) => {
//                 element.insertAdjacentHTML('afterbegin',
//                     `
//                     <div class="user-item">
//                     <label class="user-label">
//                     <input type="radio" name="user" id="${id}">
//                     <p>
//                         <span class="item-name">name</span>: ${name}
//                     </p>
//                     <p>
//                         <span class="item-name">username</span>: ${username}
//                     </p>
//                     <p>
//                             <span class="item-name">email</span>: ${email}
//                     </p>
//                     </label>
//                     </div>
//                     `
//                 )
//             });
//         })
// }
// renderUser(root)











// Exersise 2

/**
 * https://ajax.test-danit.com/api-pages/jsonplaceholder.html
 Напишите AJAX-запрос методом GET на адрес "/api/json/users/{id}/posts"
 для получения всех постов определенного юзера.
 Должны получать только те посты кторые у юзера
 HTML Пользователя
    <div class="user-item">
        <label class="user-label">
            <input type="radio" name="user" id="{ id }">
            <p>
                <span class="item-name">name</span>: { name }
            </p>
        </label>
    </div>
     HTML Поста
     <div class="post-item">
         <p class="item-title">{ title }</p>
         <p>{ body }</p>
     </div>


 Для написания кода используйте fetch.
 */



////////////////////////////////////////ТУТ ЩОСЬ НЕ ПРАЦЮЄ////////////////////////////////////////////////////ТУТ ЩОСЬ НЕ ПРАЦЮЄ

// function sendRequest(url) {
//     return fetch(url)
// }

// const containerUser = document.querySelector('#users');
// const containerPostUser = document.querySelector('#user-posts');
// const loaderUser = document.querySelector('.loader');
// const loaderPost = document.querySelector('.loader');
// const API = "https://ajax.test-danit.com/api/json/";

// function renderUsers(element, url) {
//     sendRequest(`${API}users`)
//         .then((response) => response.json()) //перетворює строку о об'єкт
//         .then((data) => {
//             data.forEach(({ id, name }) => {
//                 element.insertAdjacentHTML('beforeend', `
//                 <div class="user-item">
//                     <label class="user-label">
//                         <input type="radio" name="user" id="${id}">
//                         <p>
//                             <span class="item-name">name</span>: ${name}
//                         </p>
//                     </label>
//                 </div>
//                 `)
//             });
//         })
//         .finally(() => { loaderUser.remove() }) ///видалення при будь якій відповіді sendRequest
// }


// function renderPost(element) {
//     containerUser.addEventListener('input', (event) => {
//         let id = event.target.id;
//         sendRequest(`${API}users/${id}/posts`)
//             .then((response) => response.json())
//             .then((data) => {
//                 if (!!containerPostUsers.innerHTML) {
//                     containerPostUsers.innerHTML = ""
//                 }

//                 data.forEach(({ title, body }) => {
//                     element.insertAdjacentHTML('beforeend', `
//                     <div class="post-item">
//                         <p class="item-title">${title}</p>
//                         <p>${body}</p>
//                     </div>`)
//                 })
//             })
//             .finally(() => {
//                 loaderPost.remove()
//             })
//     })
// }

// renderUsers(containerUser)
// renderPost(containerPostUser)
////////////////////////////////////////ТУТ ЩОСЬ НЕ ПРАЦЮЄ////////////////////////////////////////////////////ТУТ ЩОСЬ НЕ ПРАЦЮЄ


//ТУТ ПРАЦЮЄ, СКОПІЙОВАНО З ГІТЛАБА
const sendRequest = (url) => {
    return fetch(url);
}

const containerUsers = document.querySelector("#users");
const containerPostUsers = document.querySelector("#users-posts");
const loaderUser = containerUsers.querySelector(".loader");
const loaderPost = containerPostUsers.querySelector(".loader");
const API = "https://ajax.test-danit.com/api/json/"

function renderUsers(element) {
    sendRequest(`${API}users`)
        .then((response) => response.json()) /// перетворює строку о об'єкт
        .then((data) => {
            // loaderUser.remove()
            data.forEach(({ id, name }) => {
                element.insertAdjacentHTML('beforeend', `
                    <div class="user-item">
                        <label class="user-label">
                            <input type="radio" name="user" id="${id}">
                            <p>
                                <span class="item-name">name</span>: ${name}
                            </p>
                        </label>
                    </div>
                `)
            });
        })
        .finally(() => {
            loaderUser.remove() /// удаление при любом ответе sendRequest.
        })

}

function renderPost(element) {
    containerUsers.addEventListener("input", (event) => {
        let id = event.target.id
        sendRequest(`${API}users/${id}/posts`)
            .then((response) => response.json())
            .then((data) => {
                if (!!containerPostUsers.innerHTML) {
                    containerPostUsers.innerHTML = ""
                }
                data.forEach(({ title, body }) => {
                    element.insertAdjacentHTML('beforeend', `
                    <div class="post-item">
                        <p class="item-title">${title}</p>
                        <p>${body}</p>
                    </div>
                    `)
                })
            })
            .finally(() => {
                loaderPost.remove()
            })
    })
}


renderUsers(containerUsers)
renderPost(containerPostUsers)





// Exercise 4

/**
 * # Завдання
 * Написати функціонал калькулятору курсу валют використовуючи API https://exchangerate.host/#/docs
 *
 * ## Технічні вимоги
 * * при успішному розрахунку результат показується у блоку нижче
 * * якщо приходять помилки, то відображати їх також нижче
 *
 * ### Advanced
 * додати можливість обчислення всіх можливих курсів валют, що надає API
 * для цього при завантаженні сторінки робити запит, щоб отримати всі можливі валюти, а потім рендерити селект компоненти з цими валютами
 * */

// const API_URL = 'https://api.exchangerate.host/';
// const endpoint = 'convert';
















// Exercise 5

/**
* # Завдання
 * Написати віджет погоди, використовуючи [Open Weather API](https://openweathermap.org/current)
 * [Open Weather widgets](https://openweathermap.org/widgets-constructor)
 *
 * ## Технічні вимоги
 * Робити запит погоди по вказаному місту та відображати інформацію в віджет на сторінці
 * Для відображення стану погоди використовувати бібліотеку іконок
 * {https://openweathermap.org/weather-conditions} http://openweathermap.org/img/wn/10d@2x.png
 * (https://erikflowers.github.io/weather-icons/)
 * Якщо приходить помилка, то відображати її текст в модальному вікні
 *
 * Parms fetch
 * units=metric - значення температури у C
* */


const API_TOKEN = '35756bfc6093a7b26cf2e72be171f555';
const API_URL = 'https://api.openweathermap.org';
const ICON_URL = 'http://openweathermap.org/img/wn/';
const icons = {
    '01d': 'weather-icon-clear-sky', /*clear sky*/
    '02d': 'weather-icon-few-clouds', /*few clouds*/
    '03d': 'weather-icon-scattered-clouds', /*scattered clouds*/
    '04d': 'weather-icon-broken-clouds', /*broken clouds*/
    '09d': 'weather-icon-shower-rain', /*shower rain*/
    '10d': 'weather-icon-rain', /*rain*/
    '11d': 'weather-icon-thunderstorm', /*thunderstorm*/
    '13d': 'weather-icon-snow', /*snow*/
    '50d': 'weather-icon-mist', /*mist*/
    '01n': 'weather-icon-night-clear-sky',/*clear sky*/
    '02n': 'weather-icon-night-few-clouds',/*few clouds*/
    '03n': 'weather-icon-night-scattered-clouds',/*scattered clouds*/
    '04n': 'weather-icon-broken-clouds',/*broken clouds*/
    '09n': 'weather-icon-night-shower-rain',/*shower rain*/
    '10n': 'weather-icon-night-rain',/*rain*/
    '11n': 'weather-icon-night-thunderstorm',/*thunderstorm*/
    '13n': 'weather-icon-night-snow',/*snow*/
    '50n': 'weather-icon-night-mist',/*mist*/
}
const MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

function sendRequest(url) {
    return fetch(url)
}

function renderWeatherWidget(url) {
    const weatherIcon = document.querySelector('.weather-icon');
    const temperature = document.querySelector('.temperature');
    const description = document.querySelector('.description');
    const city = document.querySelector('.city');
    const month = document.querySelector('#month');
    const day = document.querySelector('#day');
    const sidebar = document.querySelector('#sidebar');
    sidebar.addEventListener('input', (event) => {
        let id = event.target.id

        sendRequest(`${API_URL}/data/2.5/weather?id=${id}&units=metric&appid=${API_TOKEN}`)
            .then(response => response.json())
            .then(data => {
                // console.log(data)
                const { name, weather, dt, main } = data;
                const time = new Date(dt * 1000)
                const currentClass = icons[weather[0].icons] //це заходим в масив( дивитись консоль лог)   //weather[0].icons => 01n ==== icons["01n"] => 'weather-icon-night-clear-sky'
                if (currentClass) {
                    weatherIcon.className = 'weather-icon'  /// weather-icon + weather-icon-night-clear-sky
                }
                weatherIcon.classList.add(currentClass)
                temperature.innerHTML = Number.parseInt(main.temp) //округлюємо число
                description.innerHTML = weather[0].description
                city.innerHTML = name
                month.innerHTML = MONTHS[time.getMonth()]
                day.innerHTML = time.getDay()
            })
    })

}

renderWeatherWidget()










// Exercise 6

/**
 * # Завдання
 * Створити сторінку актуальніх фільмів, використовуючи (http://themoviedb.org/)
 * Запррос ${ API_URL }/discover/movie?api_key=${ API_KEY_3 }
 * ## Технічні вимоги
 * * Створити блок фільм в якому буде таки поля:
 * * { poster_path, title, original_title, overview, vote_average, release_date}
 *   http://joxi.ru/zANxg8gc15ZJ7m
 *
 *
<div class="film-item">
        <div class="film-poster">
        <img src="${ IMG_URL }${ poster_path }" alt="">
        </div>
        <div class="film-content">
            <p class="film-title">${ title }</p>
            <p class="film-title-original">${ original_title }</p>
            <p class="film-desc">${ overview }</p>
            <p class="film-info">
                <span>Оценка</span>${ vote_average }
                <span>Релиз</span>${ release_date }
            </p>
        </div>
</div>
 *
 * */

//1 найти все необходимые контейнеры в html (loader,posts-list)
//2 сделать функцию fetch
//3 Сделать функцию которая будет получать данные и их рендерить на странице { poster_path, title, original_title, overview, vote_average, release_date}
// const API_URL = "https://api.themoviedb.org/3";
// const IMG_URL = "https://image.tmdb.org/t/p/w500";
// const API_KEY_3 = "3f4ca4f3a9750da53450646ced312397";
// const posts = document.querySelector("#posts")

// function sendRequest(url) {
//     return fetch(url)
// }

// function renderPostFilm(element, url) {
//     sendRequest(url)
//         .then(response => response.json())
//         .then(({ results }) => {
//             const postsLoading = posts.querySelector('.loader')
//             postsLoading.remove()
//             // console.log(results);
//             results.forEach((film) => {
//                 const { poster_path, title, original_title, overview, vote_average, release_date } = film;
//                 element.insertAdjacentHTML('beforeend', `
//                 <div class="film-item">
//                     <div class="film-poster">
//                         <img src="${IMG_URL}${poster_path}" alt="">
//                     </div>
//                     <div class="film-content">
//                         <p class="film-title">${title}</p>
//                         <p class="film-title-original">${original_title}</p>
//                         <p class="film-desc">${overview}</p>
//                         <p class="film-info">
//                             <span>Оценка</span>${vote_average}
//                             <span>Релиз</span>${release_date}
//                         </p>
//                     </div>
//                 </div>
//                 `)
//             });
//         })
// }
// renderPostFilm(posts, `${API_URL}/discover/movie?api_key=${API_KEY_3}`)
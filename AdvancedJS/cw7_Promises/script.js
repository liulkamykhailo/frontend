// let promise = new Promise(function (resolve, reject) {
//     setTimeout(() => resolve(console.log('done')), 1000)

// })

// promise.then(
//     result => console.log(result),
//     error => console.log(error),
// )






// setTimeout(() => {
//     console.log('setTimiout');
// }, 0) // все рівно буде затримка навіть якщо вказано 0 сек





// const promise = new Promise(function (resolve, reject) {
//     console.log('test in promise');
//     resolve(['Kyiv'])
//     reject(new Error('error'))
// })
// promise.then(function (data) {
//     console.log('from then', data);
// }).catch(err => { console.log('from catch', err); })





// const promise = new Promise(function (res, rej) {
//     let time = 0;
//     rej(new Error('rusnia'))
//     setTimeout(function () {
//         rej(new Error('ruska federaska'))
//     }, 1000)
//     setInterval(() => {
//         console.log(++time);
//     }, 500)
// })
// promise.then(
//     result => console.log(result),
//     err => console.log(err.message),
// )





// const jsonPromise = new Promise(function (resolve, reject) {
//     resolve("This is NOT JSON!!!!!")
// })
// jsonPromise
//     .then((data) => { console.log('Done!', JSON.parse(data)) })//не виконається
//     .catch(function (err) { console.log('Pomylka!', err); })//це виконається




//Promise.all() //дочекається виконання всіх промісів, але якщо хоч в одному з них буде помилка все припиняється




// const userAge = prompt("Kurva skike rokiv?");
// const promise1 = new Promise(function (resolve, reject) {
//     setTimeout(() => {
//         if (userAge >= 18) {
//             resolve("Mojna yviyty")
//         } else {
//             reject("Malolitniy pacan idy zvidsy")
//         }
//     }, 1000)
// })
// promise1
//     .then(result => { console.log(result) })
//     .catch(err => { console.log(err); })

// console.log("After promise");















// Practice!!!!!!



// Задача 1

/**
 * productsPromise робить запит на сервер, і іноді з'являється помилка
 *
 * Якщо продукти приходять, то вивести їх списком на сторінку,
 * інакше вивести модальне вікно з ім'ям помилки (alert)
 *
 */
// const data = [
//     {
//         model: "Macbook Pro 13\"",
//         name: "Apple",
//         price: 5000,
//         vats: 20
//     },
//     {
//         model: "Microsoft 14\"",
//         name: "Microsoft",
//         price: 12000,
//         simCard: false,
//         vats: 0
//     },
//     {
//         model: "Iphone 7",
//         name: "Apple",
//         nfc: true,
//         price: 16000,
//         vats: 50
//     }
// ]


// const loading = document.querySelector('.loader')

// let productsPromise = new Promise(function (resolve, reject) {
//     setTimeout(() => {
//         let random = Math.random()

//         if (random < 0.5) {
//             resolve(data)
//         } else {
//             reject(new Error("New error"))
//         }
//     }, 1000)
// })
// productsPromise
//     .then((product) => {
//         loading.style.display = 'none';

//         let ul = document.createElement('ul');
//         document.body.append(ul)
//         product.forEach(({ name, model }) => {
//             // let li = document.createElement('li')
//             // li.innerHTML = `${name} --> ${model}`
//             // ul.append(li)

//             //варіант 2 як одразу і створити і додати
//             ul.insertAdjacentHTML('afterbegin', `<li>${name} --> ${model}</li>`)
//         });
//     })
//     .catch(err => {
//         loading.style.display = 'none';
//         const errorElement = document.createElement('p')
//         errorElement.innerHTML = err.message;
//         document.body.append(errorElement)
//     })








// Задача 2

/**
 * Доповнити функцію authenticate, яка буде резолвити успішно
 * проміс, якщо передали username: 'admin', password: '123',
 * та повертати помилку 'Invalid data' в іншому випадку
 *
 */


// user('admin', '123') // Success
// user('admiasdsadn', 'sadasdsa') // Error


// function authenticate(username, password) {
//     return new Promise((resolve, reject) => {
//         if (username === 'admin' && password === '123') {
//             resolve("its ok")
//         } else {
//             reject(new Error('Invalid data'))
//         }
//     })
// }
// function user(username, password) {
//     authenticate(username, password)
//         .then(result => console.log(result))
//         .catch(err => console.log(err.message))
// }







// Задача 3

/**
 * Написати функцію getWeather, яка приймає ім'я міста та
 * повертає проміс, який через 1 секунду зарезолвиться,
 * якщо ми передали Kyiv, Lviv, Kharkiv,
 * інакше буде помилка з текстом 'Невідоме місто'
 *
 */

// function getWeather(city) {
//     let citiesArr = ['kyiv', 'lviv', 'kharkiv'];
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             if (citiesArr.includes(city.toLowerCase())) {
//                 resolve({
//                     weather: 'hot',
//                     temperature: '+24',
//                     city //якщо хочемл аби і ключ і значення були city
//                 })
//             } else {
//                 reject(new Error(`Невідоме місто: ${city}`))
//             }
//         }, 1000)
//     })
// }
// function weatherApp(city) {
//     getWeather(city)
//         .then(({ weather, temperature, city }) => console.log(`${weather}, ${temperature}, ${city}`))
//         .catch(({ message }) => console.log(message))
// }
// weatherApp('kyiv')








// Задача 4

/**
 * Написати функціонал логіну на сайт
 * при натисканні на кнопку `Login` перевіряються введені дані
 * через 1 секунду, якщо дані валідні, то виводиться модальне вікно з текстом Success
 * інакше в блок помилок на сторінці виводиться повідомлення _Invalid data_

 ## Технічні вимоги
 * При натисканні на `Login` кнопку передається функції `authenticate` ім'я користувача та пароль
 * Через вказаний проміжок часу резолвиться або реджектиться проміс відповідно без або з помилкою
 * Використати наявний масив валідних даних для перевірки
 *
 * */
const USERS = [
    ['admin', '123'],
    ['john', 'qwe'],
    ['jack', 'asd'],
    ['marry', '123']
]



const form = document.getElementById('login-form');
const errorMessage = form.querySelector('.errors');

function login(element) {
    element.addEventListener('submit', (event) => {
        event.preventDefault()
        // console.log(event.target[0].value);
        // console.log(event.target[1].value);
        const user = event.target[0].value;
        const password = event.target[1].value;
        authenticate(user, password)
            .then(data => {
                alert(data)
                errorMessage.style.display = 'none'
            })
            .catch(({ message }) => errorMessage.innerHTML = message)
    })
}

function authenticate(name, password) {
    return new Promise((resolve, reject) => {
        USERS.forEach((user) => {
            const [userBD, passwordBD] = user;
            if (name === userBD && password === passwordBD) {
                resolve("Sucess")
            } else {
                reject(new Error("Invalid data"))
            }
        })
    })
}

login(form)


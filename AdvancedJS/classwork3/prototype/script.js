/**
 * Наявний функціонал для друку оцінок одного студента
 * Але зі збільшенням кількості студентів постало питання про
 * розширення його
 * Для цього необхідно створити функцію-конструктор Student,
 * яка буде створювати об'єкт студента та мати ті ж самі методи
 *
 * - створити за допомогою функції-конструктора ще 2х студентів
 * - вивести оцінки кожного за допомогою метода printGrades
 * - вивести середній бал кожного студента
 * - додати метод, який буде виводити оцінку по заданій технологій
 * наприклад getGrade('html') повинен виводити оцінку студента по html
 * - вивести в консоль оцінку по js першого студента та по python - третього
 *
 *
 * ADVANCED:
 * - створити окремо функцію getStudentWithHighestResults, яка буде виводити
 * ім'я та прізвище студента за найвищим середнім балом
 */


// const student1 = new Student('Stepan', 'Bandera', {javascript: 7, python: 8, html: 10, css: 9}) // 8.5
// const student2 = new Student('Andrei', 'Itov', {javascript: 10, python: 9, html: 6, java: 7})
// const student3 = new Student('Dan', 'Student', {javascript: 9, python: 8, html: 8, css: 8})
// const student4 = new Student('Dan2', 'Student2', {javascript: 7, python: 8, html: 10, css: 9})
// student1.printGrades()
// student1.averageGrade()
// student2.averageGrade()
// student3.averageGrade()
// student1.languageGrade('css')
// student1.languageGrade('asdasdsa')

// students(student1, student2, student3,student4);







function Student(name, lastName, grades) {
    this.name = name;
    this.lastName = lastName;
    this.grades = grades;
}

const student1 = new Student('Stepan', 'Bandera', { javascript: 7, python: 8, html: 10, css: 9 }) // 8.5
const student2 = new Student('Andrei', 'Itov', { javascript: 10, python: 9, html: 6, java: 7 })
const student3 = new Student('Dan', 'Student', { javascript: 9, python: 8, html: 8, css: 8 })
const student4 = new Student('Dan2', 'Student2', { javascript: 7, python: 8, html: 10, css: 9 })

Student.prototype.printGrades = function () {
    console.log(`${this.name} ${this.lastName}`);
    for (const item in this.grades) {
        console.log(`${item} : ${this.grades[item]}`); // пошук по об'єкту
    }
}
Student.prototype.averageGrade = function () {
    //варіант 1
    // const sum = 0;
    // let count = 0;
    // for (const item in this.grades) {
    //     sum += this.grades[item];
    //     count++;
    // }
    // console.log(`Середній бал: ${sum / count}`);
    //варіант 1

    //варіант 2
    let sum = Object.values(this.grades).reduce((total, anmount, index, arr) => {
        total += anmount;
        if (index === arr.length - 1) {
            return total / arr.length
        }
        return total
    }, 0)
    return sum;
    //варіант 2
}

Student.prototype.languageGrade = function (value) {
    if (!value) { return }

    console.log(`${this.name} ${this.lastName}`);
    const subj = Object.keys(this.grades)

    if (subj.includes(value.toLowerCase())) {
        console.log(`${value} ${this.grades[value]}`);
    } else {
        console.log(`${value} error`);
    }
}


student1.printGrades()
// console.log(student1.averageGrade());
// student2.averageGrade()
// student3.averageGrade()
// student1.languageGrade('css')
student1.languageGrade('asdasdsa')


function students() {
    const allStudents = Object.values(arguments);
    console.log(allStudents);
    // console.log(arguments); //не можна перебрати не ітерабельний!
    let maxGrade = 0;
    let fullName = '';
    let studentArray = [];
    allStudents.forEach((item) => {
        let currentGrade = item.averageGrade();
        if (currentGrade >= maxGrade) {
            maxGrade = currentGrade;
        }
        fullName = item.name + ' ' + item.lastName;
        studentArray.push({ fullName, grade: currentGrade })
    })
    const result = studentArray.filter((item) => item.grade === maxGrade)
    console.log(result);
}

students(student1, student2, student3, student4);










// Не працює так як нижче!!

// function Student(name, surname) {
//     this.name = name;
//     this.surname = surname;
// }

// Student.prototype = {
//     sayHi() {
//         console.log(this.name);
//     },

//     sayWoof() {
//         console.log(this.surname);
//     },
// }

// const oleg = new Student('Oleg', 'Vakarchuk');

// oleg.sayHi()
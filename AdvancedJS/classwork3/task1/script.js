/**
Напишите класс User, который будет использоваться для создания объекта, описывающего пользователя панели администрирования сайта.
У объекта должны быть такие поля:
- role (super admin, admin, main manager, content manager);
- login;
- email;
- password;

сеттеры и геттеры:
- role (можно установить роль только из списка: super admin, admin, main manager, content manager);
- login (не меньше трех букв);

а также метод:
- isValidEmail - проверят, является ли переданная строка email'ом (в ней должен быть символ @, минимум 1 точка и не
    должно быть пробелов);

Дополнительное задание: добавьте метод getPasswordStrength, который проверяет силу пароля. Метод получает
 строку (пароль) и возвращает один из трех вариантов:
- weak - меньше 6 символов, не содержит букв или цифр;
- medium - больше 6 символов, содержит цифры и буквы, но не содержит знаков: -_^$%#@*&?
- strong - больше 6 символов, содержит буквы, цифры и спецсимволы: -_^$%#@*&?
 **/


// _ використовувати get і set
class User {
    constructor() {
        this._role = null;
        this._login = null;
        this._email = null;
        this._password = null;
        this.statusPassword = null;
    }


    get role() {
        return this._role;
    }
    set role(value) {
        const roleUser = ['super admin', 'admin', 'main manager', 'content manager'];
        if (roleUser.includes(value.toLowerCase())) {
            this._role = value;
        }
    }


    get login() {
        return this._login;
    }
    set login(value) {
        if (value.length >= 3) {
            this._login = value;
        }
    }


    isValidEmail(email) {
        return email.includes('@') && email.includes('.') && !email.includes(' ')
    }
    get email() {
        return this._email
    }
    set email(value) {
        if (value) {
            this._email = value
        }
    }

    getPasswordStrength(password) {
        const numberLaters = /\d\w/ //тільки числа та букви
        const specCharters = /[-_^$%#@*&?]/g //тільки дані спецсимволи 

        if (password.length < 6 && !numberLaters.test(password)) {
            this.statusPassword = 'week'
        } else if (password.length >= 6 && numberLaters.test(password) && !specCharters.test(password)) {
            this.statusPassword = 'medium'
        } else if (password.length >= 6 && numberLaters.test(password) && specCharters.test(password)) {
            this.statusPassword = 'strong'
        } else {
            this.statusPassword = 'non valid'
        }
    }
    get password() {
        return this._password
    }
    set password(value) {
        this.getPasswordStrength(value);
        this._password = value;
    }
}

const admin = new User();
admin.role = 'admin';
console.log(admin.role);

admin.login = 'abc';
console.log(admin.login);

admin.email = "abc@gmail.com"
console.log(admin.email);

admin.password = '#nnss'
console.log(admin.password, admin.statusPassword);

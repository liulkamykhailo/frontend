/**
 * Написати клас Modal, за допомогою якого створити 2 об'єкта
 * модальних вікон: loginModal та signUpModal
 *
 * * loginModal має наступні параметри:
 * id - 'login-modal'
 * text - 'Ви успішно увійшли'
 * classList - 'modal login-modal'
 *
 * signUpModal має наступні параметри:
 * id - 'sign-up-modal'
 * text - 'Реєстрація'
 * classList - 'modal sign-up-modal'
 *
 * Кожне модальне вікно обов'язково має наступні методи:
 * - render() - генерує html код модального вікна
 * - open() - показує модальне вікно
 * - close() - закриває модальне вікно
 *
 * - За допомогою методу redner() додати html код
 * модальних вікок в кінець body
 * - При натисканні на кнопку Login за допомогою методу openModal
 * відкривати модальне вікно loginModal
 * - При натисканні на кнопку Sign Up за допомогою методу openModal
 * відкривати модальне вікно signUpModal
 *
 */


class Modal {
    constructor(id, text, classList) {
        this.id = id;
        this.text = text;
        this.classList = classList;
        this.modalWrapper = document.createElement('div');
        this.modalContent = document.createElement('div');
        this.modalClose = document.createElement('span');
    }

    render() {
        this.modalWrapper.classList.add('modal');
        this.modalContent.classList.add('modal-content');
        this.modalClose.classList.add('close');

        this.modalClose.type = 'button';
        this.modalClose.textContent = 'X';
        if (this.classList) {
            this.modalWrapper.classList.add(this.classList);
        }

        this.modalContent.append(this.modalClose, this.text);
        this.modalClose.append(this.modalContent);
        document.body.append(this.modalWrapper);

        this.modalClose.addEventListener('click', () => {
            this.close();
        });

        const openModalButton = document.querySelector(this.id);
        if (openModalButton) {
            this.modalClose.addEventListener('click', () => {
                this.open();
            });
        }
    }

    open() {
        this.modalWrapper.style.display = "block";
    }
    close() {
        this.modalWrapper.style.display = "none";
    }
}

const loginModal = new Modal('#login-modal', 'Ви успішно увійшли', 'login-modal');
loginModal.render();
const signUpModal = new Modal('#sign-up-modal', 'Реєстрація', 'sign-up-modal');
loginModal.render();

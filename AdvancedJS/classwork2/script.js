class Employee {
    constructor({ name, surName, age, salary }) {
        this.name = name;
        this.surName = surName;
        this.age = age;
        this.salary = salary;
    }
}
let i = new Employee({ name: 'a', surName: "b", age: 18, salary: 100000 });
console.log(i);
// .__proto__     працює з об'єктом
// .prototype     працює з самим классом напр Employee


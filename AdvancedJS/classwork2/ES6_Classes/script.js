// import log from "fancy-log";

/**
 *
 * За допомогою класів створити наступні об'єкти: 2 ноутбуки, 3 телефони та 1 планшет
 *
 * Вивести в консоль вартість для ринку 3х будь-яких товарів
 * Вивести в консоль вартість доставки для 3х різних товарів
 *
 * ### ADVANCED
 * Додати можливість створювати преміум телефони, які мають таку ж логіку, як і телефони, тільки націнка становить 35%
 *
 * ## Технічні вимоги
 * При створенні задається виробник, модель та ціна
 *
 * Метод getName() повинен повертати повну назву продукту:
 * ВИРОБНИК + МОДЕЛЬ, наприклад Apple Macbook Pro 13"
 *
 * Для телефонів також задається наявність NFC чипу,
 * а для планшетів наявність слоту для sim карти
 *
 * Кожен продукт має націнку 20%, додати метод, який
 * буде повертати ціну вже з націнкою
 *
 * ### Розрахунок вартості доставки
 * Кожен тип продукту (ноутбук, планшет та телефон) має свою логіку розрахунку вартості доставки:
 *
 * #### Для НОУТБУКУ
 * якщо ціна з націнкою більше 30 000, то доставка безкоштовна
 * інакше доставка коштує 200
 *
 * #### Для ПЛАНШЕТУ
 * якщо ціна з націнкою більше 20 000, то доставка безкоштовна
 * інакше ціна доставки це 1% від ціни з націнкою
 *
 * #### Для ТЕЛЕФОНУ
 * якщо ціна з націнкою більше 10 000, то доставка розраховується як 1% від загальної ціни
 * інакше береться 3%
 *
 * */
// 1. створити product class
// 2. додати параметри до класу product "name, price, vat, model"
// 3. зробити методом getName() повинен повертати повну назву продукту
// 4. getVatPrice() цена с наценкой


class Product {
    constructor(name, price, vat = 0, model) {
        this.name = name;
        this.price = price;
        this.vat = vat;
        this.model = model;
    }
    getName() {
        return `name:${this.name} model:${this.model}`;
    }
    getVatPrice() {
        return this.price + this.price * (this.vat / 100);
    }
}

class Laptop extends Product {
    constructor(name, price, vat, model) {
        super(name, price, vat, model)
    }
    delivery() {
        return this.getVatPrice() > 30000 ? 0 : 200
        //умова ? true : false
    }
}

class Tablet extends Product {
    constructor(name, price, vat, model, sim) {
        super(name, price, vat, model)
        this.sim = sim;
    }
    calcPriceOptions() {
        return this.sim ? this.price + 300 : this.price;
    }
    getVatPrice() {
        return this.calcPriceOptions() + this.price * (this.vat / 100);
    }
    delivery() {
        return this.getVatPrice() > 20000 ? 0 : this.getVatPrice() * 0.01;
    }
}


class Phone extends Product {
    constructor(name, price, vat, model, nfc) {
        super(name, price, vat, model)
        this.nfc = nfc;
    }
    calcPriceOptions() {
        return this.nfc ? this.price + 600 : this.price;
    }
    getVatPrice() {
        return this.calcPriceOptions() + this.price * (this.vat / 100);
    }
    delivery() {
        return this.getVatPrice() > 10000 ? this.getVatPrice() * 0.01 : this.getVatPrice() * 0.03;
    }
}


// const laptop = new Laptop('Lenovo', 10000, 10, 'abcNote Pro + Max Ultra');
// console.log(laptop);
// console.log(laptop.getName());
// console.log(laptop.getVatPrice());
// console.log(laptop.delivery());

const phone = new Phone('Apple', 15000, 20, 'Pro Max Ultra 1TB', false);
console.log(phone);
console.log(phone.getName());
console.log(phone.getVatPrice());
console.log(phone.delivery());

const phoneSE = new Phone('Apple', 15000, 20, 'Pro Max Ultra 1TB', true);
console.log(phoneSE);
console.log(phoneSE.getName());
console.log(phoneSE.getVatPrice());
console.log(phoneSE.delivery());
/**
 *
 * Создайте 3 объекта, каждый со своими методами и свойствами:
 *    товар - Ноутбуки,
 *    товар - Телефоны,
 *    товар - Стиральные машинки.
 *    Повторяющиеся методы и свойства вынесите в прототип.
 *
 * */


const phone = {
    name: 'Apple',
    price: 100000,
    vat: 5,
    model: "Max ultra"
};

const tablet = {
    name: 'Apple',
    price: 120000,
    vat: 7,
    model: "Max ultra pro"
};

const laptop = {
    name: 'Apple',
    price: 150000,
    model: "Max ultra mega"
}

const methods = {
    get name() {
        return `${this.name} ${this.model}`
    },

    get price() {
        return this.price
    },

    get vatPrice() {
        return this.vat ? this.price + this.price * (this.vat / 100) : this.price + this.price * 0.2;
    },
}

phone.__proto__ = methods;
tablet.__proto__ = methods;
laptop.__proto__ = methods;

console.log(phone);
console.log(phone.name);
console.log(phone.price);
console.log(phone.vatPrice);

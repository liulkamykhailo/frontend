/**
 * # Завдання 1
 *
 * Додати функціонал пошуку інформації про користувачів за ID,
 * використовуючи наявне АПІ https://ajax.test-danit.com/api-pages/jsonplaceholder.html
 *
 * ## Технічні вимоги
 * * робити запит для отримання інформації тільки по одному користувачу
 * * відображати ім'я у вигляді текстового блоку, а електронну пошту та вебсайт у вигляді посилання
 *
 * */

/**
 *  1) находим элементы для работы, находи форму и находим элемент для отображения информации
 *  2) создать функцию sendRequest для выполнния запроса
 *  3) создать функцию поиск по ИД
 *      1) при сабмите получить анные от нашего инпута
 *          1) проверить формат данных от клиента, должно быть числовое значение.
 *      2) по полученым данным выполнить соответсвующий запрос на API
 *      3) проверить ответ запроса
 *          1) если нету данных то показть сообщение что такого пользователя нету
 *          2) а если есть то отобразить данные пользователя
 * */

// const API = 'https://ajax.test-danit.com/api/json/';
// const form = document.querySelector("#form-search");
// const userInfo = document.querySelector(".user");

// function sendRequest(url, method = "GET", options) {
//     return fetch(url, { method: method, ...options })
//         .then(response => response.json());
// }

// function searchUserId(form, info) {
//     form.addEventListener("submit", (event) => {
//         event.preventDefault() //Блокуємо дефолтну поведінку, бо форма перезагружає сторінку
//         const correctInfo = event.target.id.value;
//         // console.log(correctInfo);
//         if (!!info.textContent) {
//             info.textContent = ""
//         }
//         if (!isNaN(Number(correctInfo))) {
//             sendRequest(`${API}users/${correctInfo}`) /// fetch(https://ajax.test-danit.com/api/json/users/1, {metod: "GET"})  ===>наприклад:  fetch(https://ajax.test-danit.com/api/json/users/1, {metod: "POST", body: JSON.stringify(data)})
//                 .then(data => {
//                     // console.log(data);
//                     info.textContent = data.name
//                 })
//                 .catch(err => {
//                     if (err) {
//                         info.textContent = `Користувача з ID: ${correctInfo} не існує`
//                     }
//                 })
//         } else {
//             info.textContent = `Невірний формат вводу, потрібно число ID користувача`
//         }
//         event.target.id.value = ''; //Тут ми не використали константу хоча така і є тому що це просто перезапише значення нашої константи а нам потрібно перезаписати сам елемент, через константу не працюватиме
//     })
// }
// searchUserId(form, userInfo)













/**
 * # Завдання 2
 *
 * Реалізувати функціонал форми для створення нових користувачів
 *
 * ## Технічні вимоги
 * * АПІ https://ajax.test-danit.com/api-pages/jsonplaceholder.html
 * * при натисканні на кнопку форми відправляти POST запит з введеною інформацією
 * * при успішному виконанню запиту показувати модальне вікно (alert), що користувач створений
 *
 * */

// const API = 'https://ajax.test-danit.com/api/json/';

// const userForm = document.getElementById("user-form");

// function sendRequest(url, method = "GET", options) {
//     return fetch(url, { method, ...options })
//         .then(response => response.json())
// }

// function createNewUser(form) {
//     form.addEventListener('submit', (event) => {
//         event.preventDefault()
//         const name = event.target.name.value;
//         const email = event.target.email.value;
//         const website = event.target.website.value;
//         console.log({ name, email, website });
//         const isNotEmpty = () => {
//             const length = name.trim().length >= 3 && email.trim().length >= 3 && website.trim().length >= 3;
//             // const trim = /[\s]/gim.test(name) && email.trim() !== "" && website.trim() !== ""; //  /[\s]/gim.test(text)
//             return length
//         }
//         if (isNotEmpty()) {
//             sendRequest(`${API}users`, "POST", {
//                 body: JSON.stringify({ name, email, website }), //переводимо обєкт в формат JSON, і його віддіємо серверу, бо сервер читає в JSON форматі
//             })
//                 .then(data => {
//                     console.log(data)
//                     alert(`Create user: ${data.name}`)
//                 })
//                 .catch(err => { alert(err.message) })
//         }
//     })
// }
// createNewUser(userForm)













/**
 * # Завдання 3
 * Розробити функціонал для редагування наявних постів
 *
 * ## Технічні вимоги
 * * АПІ https://ajax.test-danit.com/api-pages/jsonplaceholder.html
 * * при зміні значення в селекті отримуємо з сервера наявний пост по ID
 * * отримані дані (title та body) відображаємо в полях вводу
 * * при натисканні на кнопку Оновлення відправляти PATCH запит по вибраному ID поста
 *
 * ## Advanced
 * * при завантаженні сторінки робити запит на весь список постів та відображати існуючі варіанти в селекті
 *
 * */

const API = 'https://ajax.test-danit.com/api/json/';
const form = document.querySelector('#post-form');
const [formTitle, formBody] = form.querySelectorAll('[type="text"]');
console.log(form.querySelectorAll('[type="text"]'));
const selectorUser = document.querySelector('#users');
const selectorPost = document.querySelector('#posts');
const postWrapper = document.querySelector('#postsWrapper');

function sendRequest(url, method = "GET", options) {
    return fetch(url, { method, ...options })
        .then(response => response.json())
}

function getUser() {
    sendRequest(`${API}users`)
        .then(users => {
            users.forEach(({ id, name }) => {
                selectorUser.insertAdjacentHTML('beforeend', `<option value="${id}">${name}</option>`) //user id 1
            })
        })
}
getUser()

function getPosts(id) {
    sendRequest(`${API}users/${id}/posts`)
        .then(posts => {
            selectorPost.innerHTML = `<option value="none" disabled selected>ID поста</option>`
            posts.forEach(({ id }) => {
                selectorPost.insertAdjacentHTML('beforeend', `<option value="${id}">${id}</option>`) //post id 1
            })
        })
}

function getOnePost(postId) { //спитати в першого юзера його перший пост і отримати тайтл і боді запитуємого поста
    sendRequest(`${API}posts/${postId}`)
        .then(({ title, body }) => {
            formTitle.value = title;
            formBody.value = body;
        })
}

selectorUser.addEventListener('change', (event) => {
    const userId = event.target.value
    postWrapper.style.display = 'block'
    getPosts(userId)
})
selectorPost.addEventListener('change', (event) => {
    const postId = event.target.value
    getOnePost(postId)
})


function updatePost(id, data) {
    sendRequest(`${API}posts/${id}`, "PATCH", { body: JSON.stringify(data) })
        .then(post => { console.log(post) })
        .catch(err => { console.log(err) })
}
form.addEventListener('submit', (event) => {
    event.preventDefault()
    const postId = event.target["post-id"].value
    const titleValue = event.target.title.value
    const bodyValue = event.target.body.value
    if (titleValue.length !== 0 && bodyValue.length !== 0) {
        updatePost(postId, { title: titleValue, body: bodyValue })
    } else {
        alert("Enter correct data")
    }
})
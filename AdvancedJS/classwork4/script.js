// ЗАДАЧА 1

/**
 * Напишите функцию changeTextSize, у которой будут такие аргументы:
 * 1. Ссылка на DOM-элемент, размер текста которого нужно изменить без регистрации и sms.
 * 2. Величина в px, на которую нужно изменить текст,  возвращает функцию, меняющую размер текста на заданную величину.
 *
 * С помощью этой функции создайте две:
 * - одна увеличивает текст на 2px от изначального;
 * - вторая - уменьшает на 3px.
 *
 * После чего повесьте полученные функции в качестве обработчиков на кнопки с id="increase-text" и id="decrease-text".
 *
 */

// const increaseButton = document.querySelector('#increase-text');
// const decreaseButton = document.querySelector('#decrease-text');
// const text = document.querySelector('#text');

// let elementSize = parseInt(text.style.fontSize);

// function changeTextSize(element, fontSize) {

//     return function () {
//         let current = elementSize + fontSize;/// elementSize==14 + fontSize==2 ====16 /// elementSize==14 + fontSize==-3 ====11
//         if (current >= 12 && current <= 40) {
//             elementSize += fontSize;
//             element.style.fontSize = `${elementSize}px`
//         }
//     }
// }
// const up = changeTextSize(text, 5);
// const down = changeTextSize(text, -5);
// increaseButton.addEventListener('click', up)
// decreaseButton.addEventListener('click', down)







// ЗАДАЧА 2


/**
 *   Напишите функцию createProduct, которая будет создавать объекты, описывающие товары.
 * У товара должны быть такие свойства:
 *    - name;
 *    - fullName;
 *    - article;
 *    - price.
 *    При этом при попытке напрямую (через точку) изменить свойство price происходит его проверка на правильность:
 *    цена должна быть целым положительным числом.
 *    Если эти требования нарушаются - присвоения не произойдет.
 *    Создавать его аналог через _price #price нельзя.
 *
 *    Пример работы:
 *    const notebook = createProduct("lenovo X120S", "lenovo X120S (432-44) W", 3332, 23244);
 *    console.log(notebook.price);// выведет  23244
 *    notebook.price = -4; // присвоение не произойдет
 *    console.log(notebook.price);// выведет  23244
 *    notebook.price = 22000;
 *    console.log(notebook.price);// выведет  22000
 *
 */

// function createProduct(name, fullName, article, price) {
//     if (price > 0 && Number.isInteger(price)) {
//         return {
//             name,
//             fullName,
//             article,

//             get price() {
//                 return price
//             },
//             set price(value) {
//                 if (value > 0 && Number.isInteger(value)) {
//                     price = value
//                 }
//             }
//         }
//     } else {
//         return {}
//     }
// }

// const notebook = createProduct("lenovo X120S", "lenovo X120S (432-44) W", 3332, 23244);
// console.log(notebook);
// notebook.price = -4; // присвоение не произойдет
// console.log(notebook.price);// выведет  23244
// notebook.price = 22000;// присвоюємо нову ціну
// console.log(notebook.price);//змінить ціну














// ЗАДАЧА 3

/**
 * Створити лічильники кількості населення для 3х
 * країн: України, Німеччини та Польщі
 * Для всіх країн населення спочатку дорівнює 0
 *
 * Збільшити населення на 1_000_000 для
 * України - двічі
 * Німеччини - чотири рази
 * Польщі - один раз
 *
 * При кожному збільшенні виводити в консоль
 * 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 * ADVANCED:
 * - Кожну секунду збільшувати населення України на 100_000 та також
 * виводити в консоль 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 */

// function countPopulation(country) {
//     let population = 0;
//     return function (value = 1000000) {
//         // if (country.toUpperCase() === 'UKRAINE') {
//         //     population += value * 2;
//         //     console.log(`${country}: ${population}`);
//         // } else if (country.toUpperCase() === 'POLAND') {
//         //     population += value;
//         //     console.log(`${country}: ${population}`);
//         // } else if (country.toUpperCase() === 'GERMANY') {
//         //     population += value * 4;
//         //     console.log(`${country}: ${population}`);
//         // } else {
//         //     population += value;
//         //     console.log(`${country}: ${population}`);
//         // }

//         switch (country.toUpperCase()) {
//             case 'UKRAINE':
//                 population += value * 2;
//                 console.log(`${country}: ${population}`);
//                 break;
//             case 'POLAND':
//                 population += value;
//                 console.log(`${country}: ${population}`);
//                 break;
//             case 'GERMANY':
//                 population += value * 4;
//                 console.log(`${country}: ${population}`);
//                 break;
//             default:
//                 console.log(`Sorry, we dont know any info about country ${country}.`);
//         }
//     }
// }

// const UKRAINE = countPopulation('ukraine');
// UKRAINE();
// UKRAINE();
// UKRAINE();
// UKRAINE();
// const POLAND = countPopulation('POLAND');
// POLAND();
// const GERMANY = countPopulation('Germany');
// GERMANY();










// ЗАДАЧА 4

/**
 * Рахувати кількість натискань на пробіл, ентер,
 * шифт та альт клавіші
 * Відображати результат на сторінці
 *
 * Створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 *
 */

const enter = document.querySelector('#enter-counter')
const space = document.querySelector('#space-counter')
const shift = document.querySelector('#shift-counter')

function counterKey(element) {
    let count = 0;

    return () => {
        count++
        element.innerHTML = count
    }
}

const counterEnter = counterKey(enter)
const counterSpace = counterKey(space)
const counterShift = counterKey(shift)
window.addEventListener('keydown', (e) => {
    if (e.code === "Enter") {
        counterEnter()
    } else if (e.code === 'ShiftLeft' || event.code === "ShiftRight") {
        counterShift()
    } else if (e.code === 'Space') {
        counterSpace()
    }
})
// const style = document.getElementById('main-text').style;
// const [font, v] = style;


// const user = {};
// const person = ['a', 'b', 'c', 'd', 'e', 'f'];
// console.log("До", user);
// [, user.name, user.lastName] = person;
// console.log("Після", user);






// < !--Напишите код, который создает переменные с именем человека,
//     а также - его статус среди других пиратов-- >
// const people = "Руда борода; Капітан піратів";
// const [name, status] = people.split(';');
// console.log(name, status);








// напишите функцию getPatientStatus, которая принимает в качестве аргумент рост в см и вес в кг пациента,
//     и возвращает 2 параметра - индекс массы тела и степень ожирения.Степени ожирения при разном значении индекса массы тела:
// 0 - от 10 до 15 - анорексия;
// 1 - от 16 до 25 - норма;
// 2 - от 26 до 30 - лишний вес;
// 3 - от 31 до 35 - I степень;
// 4 - от 36 до 40 - II степень;
// 5 - от 41 и выше - III степень;
// ....
//  Используйте лучшие правила создания модульного кода

//  индекс массы тела weight / ((height / 100) ** 2)

// function getPatientStatus(height, weight) {
//     const result = weight / ((height / 100) ** 2);
//     console.log(result);
//     const diagnos = {
//         'анорексія': [10, 15],
//         'норма': [16, 25],
//         'лишний вес': [26, 30],
//         'I степень': [31, 35],
//         'II степень': [36, 40],
//         'III степень': [41,],
//     }
//     for (const key in diagnos) {
//         let [min, max] = diagnos[key];
//         if (result < 10) {
//             return 'Напевно ви людина в невагомості'
//         };
//         if (max) {
//             if (result >= min && result <= max) {
//                 return key;
//             }
//         } else {
//             return key;
//         }
//     }
// }


// console.log(getPatientStatus(180, 200))
// console.log(getPatientStatus(180, 39))
// console.log(getPatientStatus(180, 78))
// console.log(getPatientStatus(180, 65))
// console.log(getPatientStatus(180, 22))
// console.log(getPatientStatus(175, 55))
















//  Напишите функцию, которая принимает в
//     себя 2 объекта - резюме и вакансию,
//     и возвращает процент совпадения требуемых навыков(скиллов).
//     Навык совпадает если имя скилла совпадает с именем
//     в вакансии и требуемый опыт <= опыту человека в этом навыке


// const resume = {
//     name: "Илья",
//     lastName: "Куликов",
//     age: 29,
//     city: "Киев",
//     skills: [
//         {
//             name: "Vanilla JS",
//             practice: 5
//         },
//         {
//             name: "ES6",
//             practice: 3
//         },
//         {
//             name: "React + Redux",
//             practice: 1
//         },
//         {
//             name: "HTML4",
//             practice: 6
//         },
//         {
//             name: "CSS2",
//             practice: 6
//         }
//     ]
// };

// const vacancy = {
//     company: "SoftServe",
//     location: "Киев",
//     skills: [
//         {
//             name: "Vanilla JS",
//             experience: 3
//         },
//         {
//             name: "ES6",
//             experience: 2
//         },
//         {
//             name: "React + Redux",
//             experience: 2
//         },
//         {
//             name: "HTML4",
//             experience: 2
//         },
//         {
//             name: "CSS2",
//             experience: 2
//         },
//         {
//             name: "HTML5",
//             experience: 2
//         },
//         {
//             name: "CSS3",
//             experience: 2
//         },
//         {
//             name: "AJAX",
//             experience: 2
//         },
//         {
//             name: "Webpack",
//             experience: 2
//         }
//     ]
// };

// function skills(resume, vacancy) {
//     const { skills: resumeSkills } = resume;
//     const { skills: vacancySkills } = vacancy;

//     const resumeFilter = resumeSkills.filter(({ name: resumeName, practice }) => {
//         const vacancy = vacancySkills.find(({ name: vacancyName, experience }) => (
//             resumeName === vacancyName && practice >= experience
//         ))
//         return vacancy;
//     });

//     console.log('resumeSkills', resumeSkills);
//     console.log('vacancySkills', vacancySkills);
//     const result = (resumeFilter.length / vacancySkills.length) * 100;
//     return (result >= 70) ? "Okey" : "Not"
// }

// console.log(skills(resume, vacancy));















// напишите функцию, которая создает объект.
// В качестве аргументов она принимает в себя имя, фамилию, и перечень
// строк формата "имяСвоства: значение".Их может быть много.

// пример работы:
// const user = createObject("Золар", "Аперкаль", "status: глава Юного клана Мескии", "wife: Иврейн");
// console.log(user);

// user = {
//     name: "Золар",
//     lastName: "Аперкаль",
//     status: "глава Юного клана Мескии",
//     wife: "Иврейн"
// }

// Используйте оператор rest и дестрктуризацию массива

function createObject(name, lastName, ...restObject) {
    const object = {}
    restObject.forEach((item) => {
        const itemArr = item.split(': '); ///["status", " глава Юного клана Мескии"]
        object[itemArr[0]] = itemArr[1]
    })
    return { name, lastName, ...object };
}
const user = createObject("Золар", "Аперкаль", "status: глава Юного клана Мескии", "wife: Иврейн");
console.log(user);
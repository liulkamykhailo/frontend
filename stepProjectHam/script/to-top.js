arrowTop.onclick = function () {
    window.scrollTo(scrollX, 0);
};

window.addEventListener('scroll', function () {
    arrowTop.hidden = (scrollY < document.documentElement.clientHeight);
});
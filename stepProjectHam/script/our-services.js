"use strict"


//Our Services start
let ourServicesTitles = document.querySelectorAll('.our-services-tabs-title');
let ourServicesContent = document.querySelectorAll('.our-services-tabs-content');

ourServicesTitles.forEach(e => {
    e.addEventListener('click', function () {
        let title = e.getAttribute('data-services-tab');
        let currentTitle = document.querySelector(title);

        ourServicesTitles.forEach(i => {
            i.classList.remove('active-service')
        })

        ourServicesContent.forEach(j => {
            j.classList.remove('active-service')
        });

        e.classList.add('active-service')
        currentTitle.classList.add('active-service')
    })

})
// Our Services end
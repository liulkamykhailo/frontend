const photosAlt = document.querySelectorAll('[alt]');

photosAlt.forEach(e => {
    if (e.getAttribute('alt') === '') {
        e.setAttribute('alt', 'photo');
    }
});
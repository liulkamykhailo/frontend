"use strict"

let comment = document.querySelectorAll('.people-comment-text')
let peopleName = document.querySelectorAll('.people-name-prof')
let imageOne = document.querySelectorAll('.people-img-big')
let images = document.querySelectorAll('.people-img-small')
let previous = document.querySelector('.people-img-btn-prev')
let next = document.querySelector('.people-img-btn-next')
let count = 0;

function slider(elem) {
    images.forEach((e, i) => {
        if (i === elem) {
            e.classList.add('people-img-small-bottom')
            comment[i].classList.add('people-comment-active')
            peopleName[i].classList.add('people-comment-active')
            imageOne[i].classList.add('people-comment-active')
        } else {
            e.classList.remove('people-img-small-bottom')
            comment[i].classList.remove('people-comment-active')
            peopleName[i].classList.remove('people-comment-active')
            imageOne[i].classList.remove('people-comment-active')
        }
    })
}

previous.addEventListener("click", () => {
    count = (count - 1 + images.length) % images.length;
    slider(count)
})

next.addEventListener("click", () => {
    count = (count + 1) % images.length;
    slider(count)
})


images.forEach((e, i) => {
    e.addEventListener('click', () => {
        count = i;
        slider(i)
    })
})
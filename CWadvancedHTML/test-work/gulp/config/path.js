const buildFolder = `/CWadvancedHTML/test-work/dist`;
const srcFolder = `/CWadvancedHTML/test-work/src`;

//тут отримуємо імя папки нашого проекту
import * as nodePath from "path";
const rootFolder = nodePath.basename(nodePath.resolve());

export const path = {
    build: {
        files: `${srcFolder}/`
    },
    src: {
        files: `${srcFolder}/**/*.*`
    },
    watch: {},
    clean: buildFolder,
    buildFolder: buildFolder,
    srcFolder: srcFolder,
    rootFolder: rootFolder,
    ftp: ``
}
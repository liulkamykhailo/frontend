//основний модуль
import gulp from "gulp";
//імпорт шляхів
import { path } from "./gulp/config/path.js"



global.app = {
    path: path,
    gulp: gulp
}

//імпорт Задач
import { copy } from "./gulp/tasks/copy.js";

gulp.task('default', copy);
//Как по мне самый оптимальный вариант использования, так как интуитивно понятно что происходит.
import pkg from 'gulp';
import gulpSass from 'gulp-sass'; //Обработка sass/scss файлов, и перекомпиляция в css
import * as dartSass from 'sass'; //Обработка sass/scss файлов
import fileInclude from 'gulp-file-include'; // Подключение html секций
import browserSync from 'browser-sync'; //Локальный сервер что бы сразу видить свои изменения
const sass = gulpSass(dartSass);
const {task, watch, series, src, dest, parallel} = pkg;


task('styleCSS', () => {
	return src("./src/styles/**/*.scss") /// style.scss  ./src/styles/**/*.scss  все файлы с расширением .scss
		.pipe(sass.sync({
			sourceComments: false,
			outputStyle: "expanded"
		}).on('error', sass.logError)) /// scss => css  =>  style.css
		.pipe(dest("./dist/css/"))
		.pipe(browserSync.stream());
})

task('moveHTML', () => {
	return src("./src/*.html")
	.pipe(fileInclude()) 
	.pipe(dest("./dist"))
	.pipe(browserSync.stream());
})

task('moveIMG', () => {
	return src("./src/images/**/*.jpg")
		.pipe(dest("./dist/images/"));
})

task('serve', () => {
	return browserSync.init({
		server: {
			baseDir: ['dist']
		},
		port: 9000,
		open: true
	});
});
task('watchers', () => {
	watch('./src/styles/**/*.scss', parallel('styleCSS')).on('change', browserSync.reload);
	watch('./src/views/*.html', parallel('moveHTML')).on('change', browserSync.reload);
});

task('dev', series('styleCSS', 'moveHTML', 'moveIMG')) // gulp dev

task('default', series('dev', parallel('serve', 'watchers'))); // gulp



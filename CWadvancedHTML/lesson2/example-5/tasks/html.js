const {
	src,
	dest
} = require('gulp');

module.exports = function html(){
	return src("./src/*.html")
		.pipe(dest("./dist/"));
}

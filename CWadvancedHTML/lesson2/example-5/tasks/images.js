const {
	src,
	dest
} = require('gulp');

module.exports = function images(){
	return src("./src/images/**/*.{jpg,jpeg,png,svg}")
		.pipe(dest("./dist/img/"));
}

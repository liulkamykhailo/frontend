const {
	src,
	dest
} = require('gulp');

module.exports = function scripts(){
	return src("./src/js/*.js")
		.pipe(dest("./dist/js/"));
}

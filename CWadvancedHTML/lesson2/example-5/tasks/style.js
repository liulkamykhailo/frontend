const {
	src,
	dest
} = require('gulp');

module.exports = function style() {
	return src('src/css/**/*.css')
		.pipe(dest('./dist/css/'))
}
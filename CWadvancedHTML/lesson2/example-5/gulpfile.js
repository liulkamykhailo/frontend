const gulp = require('gulp');
const requireDir = require('require-dir');
const tasks = requireDir('./tasks');

exports.style = tasks.style;
exports.images = tasks.images;
exports.scripts = tasks.scripts;
exports.html = tasks.html;

exports.default = gulp.parallel(
  exports.style,
  exports.images,
  exports.scripts,
  exports.html,
)

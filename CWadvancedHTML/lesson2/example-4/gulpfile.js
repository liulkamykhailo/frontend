//Новый синтаксис который можно встретить в интернете

const { src, dest, watch, series, task } = require("gulp");

const moveCSS = () => {
	return src("./src/css/*.css")
		.pipe(dest("./dist/css/"));
}

const moveIMG = () => {
	return src("./src/image/**/*.jpg")
		.pipe(dest("./dist/images/"));
}

const watchers = () => {
	watch('./src/css/*.css', moveCSS);
	watch('./src/image/**/*.jpg', moveIMG);
}

const dev = series(moveCSS, moveIMG, watchers);

// gulp.task("watchers", watchers);
exports.watchers = watchers;
exports.moveCSS = moveCSS;
exports.moveIMG = moveIMG;
exports.dev = dev;


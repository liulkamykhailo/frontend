// "use strict"

// window.alert('Alert');

// function showAlert() {
//     alert("Alert")
// }
// showAlert()
// window.showAlert()


// console.log("height", window.innerHeight);
// console.log("width", window.innerWidth);


// console.log(document.documentElement); //повністю весь наш HTML
// console.log(document.body);
// console.log(document.head);



// console.log(document.body.childNodes);
// console.log(document.body.firstChild);
// console.log(document.body.lastChild);
// console.log(document.body.parentNode); //виводить той в чому він обернений, body огорнений в html
// console.log(document.head.nextSibling.nextSibling); //отримуємо body
// console.log(document.body.previousSibling.previousSibling); //отримаємо head



// console.log(document.body.children); // самі елементи
// console.log(document.body.firstElementChild);
// console.log(document.body.lastElementChild);
// console.log(document.body.parentElement);
// console.log(document.head.nextElementSibling);
// console.log(document.body.previousElementSibling);



// document.body.style.background = 'red';
// setTimeout(() => (document.body.style.background = "blue"), 1000);
// setTimeout(() => (document.body.style.background = "yellow"), 2000);
// setTimeout(() => (document.body.style.background = "pink"), 3000);
// setTimeout(() => (document.body.style.background = "violet"), 4000);
// setTimeout(() => (document.body.style.background = "green"), 5000);
// setTimeout(() => (document.body.style.background = ""), 6000);


// const byClassName = document.getElementsByClassName('li');
// console.log(byClassName);
// // byClassName[0].style.background = "red" //змінюємо для першої лішки колір
// byClassName[0].remove() // видаляємо повністю елемент



// const getByQuery = document.querySelector(".li"); //повертає перший знайдений
// console.log(getByQuery);
// const getByQueryN = document.querySelector("li:nth-child(2)");
// console.log(getByQueryN);
// const getByQueryAll = document.querySelectorAll(".li"); //повертає всі з цим класом
// console.log(getByQueryAll);
// console.log(getByQueryAll[1]);
// const getElementsByName = document.getElementsByName("test-name")
// console.log(getElementsByName);



// const ulFirstLi = document.querySelector("li:nth-child(1)")
// console.log(ulFirstLi.hasAttribute('id'));
// console.log(ulFirstLi.getAttribute('id'));
// ulFirstLi.setAttribute('id', 'new-id'); //перезаписуємо значення
// console.log(ulFirstLi.getAttribute('id'));
// console.log(ulFirstLi.removeAttribute('id')); //видаляємо атрибут
// console.log(ulFirstLi.attributes); //дивимося всі атрибути що є в цього елемента



// const secondLi = document.getElementsByClassName('li li-2')[0];
// console.log(secondLi);
// secondLi.style.background = "red";
// secondLi.style.cssText = `
//     background-color: blue;
//     color: white;
//     text-align: center;
// `;




// const secondLi = document.getElementsByClassName('li li-2')[0];
// secondLi.className = "new-class one-more another-one-class" //перезаписує всі наші класси що були
// secondLi.classList.add('add-one-class') //додає один класс до тих що є
// secondLi.remove('li-2') //видаляє один класс
// secondLi.classList.toggle("li-2"); //якщо є класс то видаляє, якщо нема то додає (тут видаляє бо він є)
// secondLi.classList.toggle("li-2"); //якщо є класс то видаляє, якщо нема то додає (а тут додасть бо в попередньому ми видалили)
// secondLi.classList.replace('li-2', 'li-2-2') // бере перше значення і замінює другим
// console.log(secondLi.classList.contains('li-2')); //true
// console.log(secondLi.classList.contains('li-2312')); //false





// const secondLi = document.getElementsByClassName('li li-2')[0];
// console.log('innerText: \n', secondLi.innerText);
// console.log('innerHTML: \n', secondLi.innerHTML);
// console.log('outerHTML: \n', secondLi.outerHTML);
// console.log('textContent: \n', secondLi.textContent);

// secondLi.innerText = "<div>new text</div>"       //додає просто текстову частину така як є в лапках, заміняє попередній
// secondLi.textContent = "<div>new text</div>"       //додає просто текстову частину така як є в лапках, заміняє попередній
// secondLi.innerHTML = "<div>new text</div>"       //додає HTML розмітку , додає всередину, вирізає контент що там був
// secondLi.outerHTML = "<div>new text</div>"       //повністю замінює нашу li тим текстом що ми йому написали
















/**
 * Завдання 1.
 *
 * Отримати та вивести в консоль такі елементи сторінки:
 * - за ідентифікатором (id): елемент з ідентифікатором list;
 * - За класом - елементи з класом list-item;
 * - По тегу - елементи з тегом li;
 * - За CSS селектором (один елемент) - третій li з усього списку;
 * - За CSS селектором (багато елементів) - всі доступні елементи li.
 *
 * Вивести в консоль і пояснити властивості елемента:
 * - innerText;
 * - innerHTML;
 * - outerHTML.
 */

// const elementById = document.getElementById('list');
// const elementById2 = document.querySelector('#list');
// console.log(list); //виводимо просто одразу саму айді
// console.log(elementById);


// const elementByClass = document.getElementsByClassName('list-item');
// console.log(elementByClass);

// const elementsByTagName = document.getElementsByTagName("li");
// console.log(elementsByTagNames);

// const querySelector = document.querySelector('li:nth-child(3)');
// console.log(querySelector);

// const allLi = document.querySelectorAll("li");
// console.log(allLi);

// console.log(elementById.innerText);
// console.log(elementById.innerHTML);
// console.log(elementById.outerHTML);




/**
 * Завдання 2.
 *
 * Отримати елемент із класом .remove.
 * Видалити його з розмітки.
 *
 * Отримати елемент із класом .bigger.
 * Замінити йому CSS-клас .bigger на CSS-клас .active.
 *
 * Умови:
 * - Другу частину завдання вирішити у двох варіантах: в один рядок та в два рядки.
 */


// const remove = document.querySelector('.remove');
// console.log(remove.remove());
////////або так
// const remove = document.getElementsByClassName('remove');
// console.log(remove[0].remove());

// const bigger = document.querySelector('.bigger')
// console.log(bigger.classList.replace("bigger", "active"));
////////або так
// const bigger = document.querySelector('.bigger')
// console.log(bigger.classList.remove('bigger'));
// console.log(bigger.classList.add('active'));



/**
 * Завдання 3.
 *
 * На екрані вказано список товарів із зазначенням назви та кількості на складі.
 *
 * Знайти товари, які закінчилися та:
 * - Змінити 0 на «закінчився»;
 * - Змінити колір тексту на червоний;
 * - Змінити жирність тексту на 600.
 *
 * Вимоги:
 * - Колір елемента змінити за допомогою модифікації атрибуту style.
 */


// let arrayOfLi = document.getElementsByTagName('li')
// // console.log(arrayOfLi);
// for (const i of arrayOfLi) {
//     // console.log(i.innerText);
//     if (i.innerText.includes(': 0')) {
//         i.innerText = i.innerText.replace('0', 'закінчився')
//         i.style = `color: red; font-weight: 600`
//     }
// }

//РІШЕННЯ 2 СПОСОБОМ!!!!!!
let arrayOfLi = document.querySelectorAll('li');
//getElementsByTagName не працює!!!!!!(живі і не живі колекції)
console.log(arrayOfLi);
arrayOfLi.forEach(element => {
    if (element.innerText.includes(": 0")) {
        element.innerText = element.innerText.replace('0', 'закінчився')
        element.style = `color: red; font-weight: 600`
    }
});





/**
 * Завдання 4.
 *
 * Отримати елемент із класом .list-item.
 * Відібрати елемент із контентом: «Item 5».
 *
 * Замінити текстовий вміст цього елемента на посилання, вказане в розділі «дано».
 *
 * Зробити це так, щоб новий елемент у розмітці не було створено.
 *
 * Потім відібрати елемент із контентом: «Item 6».
 * Замінити вміст цього елемента на таке ж посилання.
 *
 * Зробити це так, щоб у розмітці було створено новий елемент.
 *
 * Умови:
 * - обов'язково використовувати метод для перебору;
 * - Пояснити різницю між типом колекцій: Array та NodeList.
 */

/* Дано */
// const targetElement = '<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google it!</a>';




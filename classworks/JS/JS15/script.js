// document.addEventListener("keydown", (event) => {
//   const name = event.key;
//   const code = event.code;
//   console.log(`key ${name} value ${code}`);
// });

// document.addEventListener("keyup", (event) => {
//   const name = event.key;
//   const code = event.code;
//   console.log(`key ${name} value ${code}`);
// });

// window.onbeforeunload = function () {
//   console.log("Close page console log");
//   return "string";
// };











/**
 * Завдання 1.
 *
 * Створити елемент h1 з текстом «Натисніть будь-яку клавішу».
 *
 * При натисканні будь-якої клавіші клавіатури змінювати текст елемента h1 на:
 * «Натиснена клавіша: ІМ'Я_КЛАВИШІ».
 */

// const h1 = document.createElement("h1");
// h1.innerText = "Press button";
// document.body.append(h1);
// window.addEventListener("keydown", (event) => {
//     console.log("event key:    ", event.key);
//     console.log("event code:   ", event.code);
//     if (event.code === "Escape") {
//         h1.innerText = "Press button";
//     } else {
//         h1.innerText = `Натиснена клавіша ${event.key}`;
//     }
// });










/**
 * Рахувати кількість натискань на пробіл, ентер,
 * та Backspace клавіші
 * Відображати результат на сторінці
 *
 * ADVANCED: створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 * Наприклад виклик функції
 * createCounter('Enter');
 * реалізовує логіку підрахунку натискання клавіші Enter
 * та відображає результат в enter-counter блок
 *
 */

// let enterCounter = 0;
// let spaceCounter = 0;
// let backspaceCounter = 0;

// window.addEventListener('keyup', (e) => {
//     if (e.code === "Enter") {
//         document.querySelector("#enter-counter").innerText = enterCounter++;
//     } else if (e.code === "Space") {
//         document.querySelector("#space-counter").innerText = spaceCounter++;
//     } else if (e.code === "Backspace") {
//         document.querySelector("#backspace-counter").innerText = backspaceCounter++;
//     } else if (e.code === "Escape") {
//         enterCounter = 0;
//         spaceCounter = 0;
//         backspaceCounter = 0;
//         document.querySelector("#enter-counter").innerText = 0;
//         document.querySelector("#space-counter").innerText = 0;
//         document.querySelector("#backspace-counter").innerText = 0;
//     }
// })














/**
 * При натисканні shift та "+"" одночасно збільшувати
 * шрифт сторінки на 1px
 * а при shift та "-" - зменшувати на 1px
 *
 * Максимальний розмір шрифту - 30px, мінімальний - 10px
 *
 */


// let fontSize = 16;

// window.addEventListener("keydown", e => {
//     console.log(e);
//     // console.log("event key:    ", e.key);
//     // console.log("event code:   ", e.code);
//     if (e.shiftKey && e.key === "+") {
//         if (fontSize <= 30) {
//             document.body.style.fontSize = `${fontSize++}px`;
//         }
//     } else if (e.shiftKey && e.code === "NumpadSubtract") {
//         if (fontSize >= 10) {
//             document.body.style.fontSize = `${fontSize--}px`;
//         }
//     }
// })













/**
 * При натисканні на enter в полі вводу
 * додавати його значення, якщо воно не пусте,
 * до списку задач та очищувати поле вводу
 *
 * При натисканні Ctrl + D на сторінці видаляти
 * останню додану задачу
 *
 * Додати можливість очищувати весь список
 * Запустити очищення можна двома способами:
 * - при кліці на кнопку Clear all
 * - при натисканні на Alt + Shift + Backspace
 *
 * При очищенні необхідно запитувати у користувача підтвердження
 * (показувати модальне вікно з вибором Ok / Cancel)
 * Якщо користувач підвердить видалення, то очищувати список,
 * інакше нічого не робити зі списком
 *
 */










// Додати у Html кнопку ScrollToTop.
// За дефолтом кнопку не видно.
// Коли користувач проскролить більше, ніж висота вікна (window.innerHeight) - показати кнопку.
// Кнопка має бути фіксована внизу сторінки.
// По кліку неї - треба проскролить на початок документа.
// (https://developer.mozilla.org/ru/docs/Web/API/Window/scrollTo)


const button = document.createElement('button');
button.innerText = "Button to top"
button.classList.add('button')
button.classList.add('button-in-active')
document.body.append(button)

window.addEventListener('scroll', e => {
    console.log(window.pageYOffset);
    if (window.pageYOffset > 1000) {
        button.classList.remove('button-in-active')
    } else if (window.pageYOffset < 1000) {
        button.classList.add("button-in-active")
    }
})

button.addEventListener('click', () => {
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    })
})
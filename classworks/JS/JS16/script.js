// prevent default
// відміняє поведінку за замовчуванням

// const a = document.querySelector('a');
// a.addEventListener("click", e =>
//     e.preventDefault());

// document.querySelector('input').addEventListener('keydown', e => {

// })



// stop propagation
// Зупинка всплития та погружения
// e.stoppropagation()




// Делегування!  Delegation
// event.target
// const board = document.querySelector(".board")













// На кліку на картинку - створювати дубль цієї картинки, наприкінці списку картинок.
// Використовувати делегування подій

// document.body.addEventListener('click', e => {
//     // console.log(e.target);
//     // console.log(e.target.getAttribute('src'));
//     if (e.target.tagName === 'IMG') {
//         // console.log('IMG');
//         let picture = document.createElement('img');
//         picture.setAttribute('src', e.target.getAttribute('src'));
//         document.body.append(picture)
//     }
// })




// Дані інпути. Зробіть так, щоб усі інпути при втраті фокусу перевіряли свій вміст на правильну кількість символів.
// Скільки символів має бути в інпуті, Вказується в атрибуті data-length.
// Якщо вбито правильну кількість, то бордер інпуту стає зеленим, якщо неправильне – червоним.


// const inputs = document.querySelectorAll('.input');

// document.body.addEventListener('click', e => {
//     if (e.target.tagName === 'INPUT') {
//         console.log(e.target.value);

//         e.target.addEventListener('blur', () => {
//             if (e.target.value.length == e.target.getAttribute('data-length')) {
//                 console.log("green");
//                 e.target.style.border = '10px solid green'
//             } else {
//                 e.target.style.border = '10px solid red'
//             }
//         })
//     }
// })






/**
 * Завдання.
 *
 * Необхідно "оживити" навігаційне меню за допомогою JavaScript.
 *
 * При натисканні на елемент меню додавати до нього CSS-клас .active.
 * Якщо такий клас вже існує на іншому елементі меню, необхідно
 * з того, що попереднього елемента CSS-клас .active зняти.
 *
 * Кожен елемент меню — це посилання, що веде на google.
 * За допомогою JavaScript необхідно запобігти перехід по всіх посилання на цей зовнішній ресурс.
 *
 * Умови:
 * - У реалізації обов'язково використовувати прийом делегування подій (на весь скрипт слухач має бути один).
 */


// const ul = document.getElementById('navList');
// let activeElement = null; //змінна в якій зберігається той елемент який активний зараз
// ul.addEventListener('click', e => {
//     e.preventDefault()
//     if (activeElement) {
//         activeElement.classList.remove('active')
//     }
//     e.target.classList.add('active')
//     activeElement = e.target
// })




// Напишіть код, який при натисканні на будь-який div всередині root буде виводити в консоль його id.


// console.log(document.getElementById('id3'));
// console.log(id3);
// id1.addEventListener('click', e => {
//     e.stopPropagation()
//     console.log("1");
// })
// id2.addEventListener('click', e => {
//     e.stopPropagation()
//     console.log("2");
// })
// id3.addEventListener('click', e => {
//     e.stopPropagation()
//     console.log("3");
// })
























// Дана таблиця з користувачами із двома колонками: ім'я та прізвище.
// Під таблицею зробіть форму, з допомогою якої можна буде додати нового користувача таблицю.
// Зробіть так, щоб при натисканні на будь-яку комірку з'являвся prompt, за допомогою якого можна змінити
//Текст комірки. Завдання вирішіть за допомогою делегування (тобто подія має бути навішана на table).

// const table = document.querySelector('.table');
// const nameContainer = document.querySelector('.name-container');
// const surnameContainer = document.querySelector('.surname-container');
// const inputName = document.querySelector('.input-name');
// const inputSurame = document.querySelector('.input-surname');
// const button = document.getElementById('button');


// button.addEventListener('click', e => {
//     const nameP = document.createElement('p');
//     const surnameP = document.createElement('p');

//     nameP.innerText = inputName.value
//     surnameP.innerText = inputSurame.value

//     nameContainer.append(nameP);
//     surnameContainer.append(surnameP);
// })

// table.addEventListener('click', e => {
//     const newWalue = prompt('Enter new walue')
//     e.target.innerText = newWalue;
// })
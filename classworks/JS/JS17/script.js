// let p = document.createElement('p');
// p.textContent = 3;
// document.body.append(p);

// let count = 3;
// let timer = setInterval(counter, 1000)
// setTimeout(() => { clearTimeout(timer) }, 4000) //видаляємо через 4 секунди, у ф-ції намагався через іф
// function counter() {
//     console.log(count);
//     p.textContent = count--
//     // if (count === -1) {
//     //     let p2 = document.createElement('p');
//     //     p2.textContent = 'End count';
//     //     document.body.append(p2);
//     //     clearTimeout(timer)
//     // }
// }


// localStorage.setItem('name', 'domestos');
// // localStorage.removeItem('name');
// localStorage.setItem('age', '27');


// console.log(localStorage);
// console.log(localStorage.getItem('age'));
// console.log(localStorage.key(0));







/**
 * Завдання.
 *
 * Зімітувати завантаження даних.
 *
 * При відкритті сторінки показувати спіннер протягом 3 секунд.
 *
 * Потім відобразити мок.
 * мок -це дані для тестового запуску
 */

// let loader = document.querySelector('.loader-wrapper')

// setTimeout(() => {
//     loader.style.display = 'none'
// }, 3000);
// setTimeout(() => {
//     loader.classList.replace('loader-wrapper', 'hidden')
// }, 3000);



//  * Завдання 2
// Створити таймер. По кліку на кнопку start - починати відлік.
// По кліку на кнопку finish - обнулять.
// Таймер вивести на сторінку


// МІЙ ВАРІАНТ

// let div = document.querySelector('.time')
// let count = 0
// let startCount
// begin.addEventListener('click', () => {
//     startCount = setInterval(() => {
//         div.textContent = count++
//     }, 1000)
// })
// finish.addEventListener('click', () => {
//     clearTimeout(startCount)
// })


// ВАРІАНТ 2
const buttonStart = document.querySelector('#begin')
const buttonFinish = document.querySelector('#finish')
const buttonPause = document.querySelector('#pause')
let timer = document.querySelector('.time');
let min = 0;
let sec = 0;
timer.innerText = `${min}:${sec}`;
let timers = null;
buttonStart.addEventListener('click', () => {
    timers = setInterval(() => {
        if (sec >= 59) {
            sec = 0
            min++
        }
        timer.innerText = `${min}:${sec++}`;
    }, 1000)
})
buttonPause.addEventListener('click', () => {
    clearInterval(timers)
})
buttonFinish.addEventListener('click', () => {
    clearInterval(timers)
    min = 0;
    sec = 0;
    timer.innerText = `${min}:${sec}`;
})













/**
 * Завдання. 3
 *
 * Написати програму для нагадувань.
 *
 * Усі модальні вікна реалізувати через alert.
 *
 * Умови:
 * - Якщо користувач не ввів повідомлення для нагадування - вивести alert з повідомленням "Ведіть текст нагадування";
 * - Якщо користувач не ввів значення секунд, через скільки потрібно вивести нагадування -
 * вивести alert з повідомленням «Час затримки має бути більше однієї секунди.»;
 * - Якщо всі дані введені правильно, при натисканні на кнопку «нагадати» необхідно її блокувати так,
 * щоб повторний клік став можливим після повного завершення поточного нагадування;
 * - після цього повернути початкові значення обох полів;
 * - Створювати нагадування, якщо всередині одного з двох елементів input натиснути клавішу Enter;
 * - Після завантаження сторінки встановити фокус у текстовий input.
 */


// let reminder = document.querySelector('#reminder');
// let seconds = document.querySelector('#seconds');
// let button = document.querySelector('button');

// button.addEventListener('click', () => {
//     button.setAttribute('disabled', '')

//     setTimeout(() => {
//         alert(reminder.value)
//         button.removeAttribute('disabled')
//     }, seconds.value * 1000)

// })







/**
 * Завдання 1.
 *
 * При завантаженні сторінки подивитися - чи є в localStorage значення ключа userName?
 * Якщо є - виводити повідомлення "Hello, userName" на екран,
 * де замість userName має бути вставлено значення за однойменним ключем в localStorage
 * Якщо значення за таким ключем немає - запитати у користувача ім'я, модальним вікном, записати його в localStorage.
 * Після цього вивести повідомлення "Hello, userName" на екран,
 * де замість userName має бути вставлено значення за однойменним ключем в localStorage
 */

// console.log(localStorage.getItem('userName'));
// let userName = localStorage.getItem('userName')
// if (!userName) {
//     let value = prompt('Your name')
//     localStorage.setItem('userName', value)
//     alert(`Hello, ${value}`)

// } else {
//     alert(`Hello, ${userName}`)
// }
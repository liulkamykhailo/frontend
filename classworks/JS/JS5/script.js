// let userPrompt = prompt();
// let user = {
//     name: userPrompt,
//     age: "",
// };

// console.log(user);
// console.log(user[userPrompt]);
// console.log(user);




/**
 * Завдання 1.
 *
 * Створити об'єкт користувача, який має три властивості:
 * - Ім'я;
 * - Прізвище;
 * - Професія.
 *
 * А також одним методом sayHi, який виводить у консоль повідомлення 'Привіт.'.
 */



// const user = {
//     name: "Misha",
//     lastName: "Liuka",
//     profession: "Front End developer",
//     sayHi() {
//         console.log(`Hello dgh ${this.name}`)
//     },
// }


// user.sayHi();

/**
 * Завдання 2.
 *
 * Розширити функціонал об'єкта з попереднього завдання:
 * - Метод sayHi повинен вивести повідомлення: «Привіт. Мене звуть Ім'я Прізвище.»;
 * - Додати метод, який змінює значення зазначеної властивості об'єкта.
 *
 * Просунута складність:
 * Метод має бути «розумним» — він генерує помилку під час спроби
 * Зміни значення неіснуючого в об'єкті якості.
 */

// const user = {
//     name: "Misha",
//     lastName: "Liuka",
//     profession: "Front End developer",
//     sayHi() {
//         console.log(`Hello ${this.name} ${this.lastName}`)
//     },

//     changeValue(key, value) {
        
//         if (this[key] !== undefined) {
//             this[key] = value
//         } else {
//             console.log("Error");
//         }
//     }
// }


// user.sayHi();
// user.changeValue("name", "ASddsad");
// console.log(user);

/**
 * 
 * Завдання 3.
 *
 * Розширити функціонал об'єкта з попереднього завдання:
 * - Додати метод, який додає об'єкту нову властивість із зазначеним значенням.
 *
 * Просунута складність:
 * Метод має бути «розумним» — він генерує помилку при створенні нової властивості
 * Властивість з таким ім'ям вже існує.
 */




/*
 * Завдання 4.
 *
 *  Створити об'єкт danItStudent,
 * У якого такі властивості: ім'я, прізвище, кількість зданих домашніх работ.
 *
 * Запитати у користувача "Що ви хочете дізнатися про студента?"
 * Якщо користувач запровадив " name " чи " ім'я " - вивести у консоль ім'я студента.
 * Якщо користувач ввів "lastName" або "прізвище" - вивести до консольпрізвища студента.
 * Якщо користувач запровадив " оцінка " чи " homeworks " - вивести у консоль кількість зданих робіт.
 * Якщо ввів, щось не з перерахованого - вивести в консоль - "Вибачте таких даних немає"
 */


// const danItStudent = {
//     userName: "Mykhailo",
//     lastName: "Liulka",
//     homeWorkAge: 16,
//     danIt() {
//         // let about = prompt("Що ви хочете дізнатися про студента?").toUpperCase();
//         // if (about === ("userName").toUpperCase()) {
//         //     console.log(this.userName);
//         // }else if(about === "lastName"){
//         //     console.log(this.lastName);
//         // } else {
            
//         // }



//         //Варіант номер 2
//         // let about = prompt("Що ви хочете дізнатися про студента?");
//         // if (this[about] === undefined) {
//         //     console.log("Sorry I don't understand");
//         // } else {
//         //     console.log(this[about]);
//         // }



//          //Варіант номер 3
//         let about = prompt("Що ви хочете дізнатися про студента?");
//         this[about] === undefined ? console.log("Sorry I don't understand") : console.log(this[about]);
//     }
// }
// danItStudent.danIt();


/*
 * Завдання 5.
 *
 *  Запитати в користувача ім'я, прізвище, вік (перевірити на правильну цифру
 * і що число між 15 і 100), чи є діти, якого кольору очі.
 * Усі ці дані записати в об'єкт.
 * Перебрати об'єкт і вивести у консолі фразу
 * `Властивість ${name}: ${значення name}` і так з усіма властивостями
 */


const user = {

    getUserInfo() {
        let name = prompt('What is your name?');
        let surname = prompt("What is your surname");
        let age;
        do {
            age = +prompt('what is your age');
            console.log(isNaN(age));
            console.log(age < 15);
            console.log(age > 100);
        }while (isNaN(age) || (age < 15 || age > 100))
        let kids = prompt("Do you have kids?");
        let eyesColor = prompt("what color of your eyes");

        this.name = name;
        this.surname = surname;
        this.age = age;
        this.kids = kids;
        this.eyesColor = eyesColor;

        console.log(this);

        for (let key in this) {
            console.log(`Властивість ${key}: ${this[key]}`);
        }
    }
}

user.getUserInfo()
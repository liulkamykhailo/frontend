/**
 * Завдання 1.
 *
 * Який буде результат обчислення?
 *
 * "" + 1 + 0                //10
 * "" - 1 + 0                //-1
 * true + false              //1
 * 6 / "3"                   //2
 * "2" * "3"                 //6
 * 4 + 5 + "px"              //9px
 * "$" + 4 + 5               //$45
 * "4" - 2                   //2
 * 4px" - 2                  //NaN
 * "  -9  " + 5              //-9   5 
 * "  -9  " - 5              //-14
 * null + 1                  //1
 * undefined + 1             //NaN
 * true + 1                  //2
 */


/**
 * Завдання 2.
 *
 * Використовуючи if..else, напишіть код, що отримує число за допомогою prompt і потім виводить повідомлення alert:
 * 1, якщо значення більше нуля,
 * -1, якщо меньше нуля,
 * 0, якщо дорівнює нулю.
 * У цьому завданні ми припускаємо, що введенне значення завжди є числом.
 *
 */

// let number = +prompt("Your number");

// function num(number) {
//     let num = number ? number : prompt("Enter again");
//     if (num > 0) {
//     alert("1")
// } else if(num < 0){
//     alert("-1")
// } else {
//     alert("0")
// }
// }
// num(5);
// num();
// if (number > 0) {
//     alert("1")
// } else if(number < 0){
//     alert("-1")
// } else {
//     alert("0")
// }


/**
 * Завдання 3.
 *
 * Перепишіть if..else, використовуючи декілька тернарних операторів '?'.
 *
 */

// const login = "Працівник";
// const login = "Директор"
// const login = ""



// if (login == "Працівник") {
//   message = "Привіт";
// } else if (login == "Директор") {
//   message = "Вітаю";
// } else if (login == "") {
//   message = "Немає логіну";
// } else {
//   message = "";
// }

// let message = login === "Працівник" ? "Привіт" : login == "Директор" ? "Вітаю" : login == "" ? "Немає логіну" : '';
// console.log(message);

/**
 * Завдання 4.
 *
 * За допомогою циклу вивести в консоль всі непарні числа,
 * які перебувають у діапазоні від 0 до 100.
 *
 */

// for (let i = 0; i < 100; i++) {
//     if (i%2 !== 0) {
//         console.log(i);
//     }
// }

// function count(start, end) {
//     for (let i = start; i < end; i++) {
//     if (i%2 !== 0) {
//         console.log(i);
//     }
// }
// }
// count(10,20)

/**
 * Завдання 5.
 *
 * Написати програму-калькулятор.
 *
 * Програма запитує у користувача три значення:
 * - Перше число;
 * - Друге число;
 * - Математична операція, яку необхідно здійснити над введеними числами.
 *
 * Програма повинна повторно запитувати дані, якщо:
 * - Будь-яке необхідних чисел не відповідають критерію коректного числа;
 * - Математична операція не є однією з: +, -, /, *, **.
 *
 * Якщо всі дані введені правильно, програма обчислює зазначену операцію, і виводить у консоль результат:
 * «Над числами ЧИСЛО_1 і ЧІСЛО_2 була проведена операція ОПЕРАЦІЯ. Результат: РЕЗУЛЬТАТ.
 *
 * Після першого успішного виконання програма повинна запитати, чи є необхідність виконатися ще раз,
 * і повинна починати свою роботу спочатку доти, доки користувач не відповість "Ні.".
 *
 * Коли користувач відмовиться продовжувати роботу програми, програма виводить повідомлення:
 * «✅ Робота завершена.».
 */

// function calculator() {
//     let n1 = +prompt("n1");
//     let n2 = +prompt("n2");
//     let operation = prompt("n3");

//     let operators = ["+", "-", "/", "*", "**"];

//     while (!operators.includes(operation)) {
//         operation = prompt("enter operation")
//     }
//     let result
// switch (operation) {
//     case "+":
//         result = n1 + n2
//         break;
//     case "-":
//         result = n1 - n2
//         break;
//     case "/":
//         result = n1 / n2
//         break;
//     case "*":
//         result = n1 * n2
//         break;
//     case "**":
//         result = n1 ** n2
//         break;

//     default:
//         break;
//     }
//     alert(`Над числами ${n1} і ${n2} була проведена операція ${operation}. Результат: ${result}`)

//     confirm("continue?") ? calculator() : alert("✅ Робота завершена.")
//     // if (question) {
//     //     calculator()
//     // } else {
//     //     alert("✅ Робота завершена.")
//     // }
// }
// calculator()

/**
 * Завдання 6.
 *
 * Користувач вводить 3 числа.
 * Вивести в консоль повідомлення з максимальним числом із введених.
 *
 * Якщо одне із введених користувачем чисел не є числом,
 * вивести повідомлення: «⛔️ Помилка! Одне з введених чисел не є числом..
 *
 */

// let n1 = +prompt("N1");
// let n2 = +prompt("N2");
// let n3 = +prompt("N3");
// function max(n1, n2, n3) {
//     if (typeof (n1, n2, n3) !== "number") {
//         console.error("⛔️ Помилка! Одне з введених чисел не є числом.");
//     }
//     return console.log(Math.max(n1, n2, n3));
// }
// max(n1, n2, n3)


// function correctNumber(first,second,third) {
//     // console.log(correctNumber.arguments);
//     let numberUser = Object.values(correctNumber.arguments);
//     Object.values(numberUser).forEach((element) => {
//         if (isNaN(parseInt(element))) {
//             throw new Error("⛔️⛔️⛔️")
//         }
//     });
//     // return console.log(Math.max(first, second, third));
//     return console.log(numberUser.sort((a, b) => b - a)[0]);
// }
// correctNumber(11, 1, 100)

/*
 * Завдання 7.
 *
 *  Створити об'єкт danItStudent,
 * У якого такі властивості: ім'я, прізвище, кількість зданих домашніх работ.
 *
 * Запитати у користувача "Що ви хочете дізнатися про студента?"
 * Якщо користувач запровадив " name " чи " ім'я " - вивести у консоль ім'я студента.
 * Якщо користувач ввів "lastName" або "прізвище" - вивести до консольпрізвища студента.
 * Якщо користувач запровадив " оцінка " чи " homeworks " - вивести у консоль кількість зданих робіт.
 * Якщо ввів, щось не з перерахованого - вивести в консоль - "Вибачте таких даних немає"
 */

// let danItStudent = {
//     name: "Mykhailo",
//     surname: "Liulka",
//     works: 7,

//     ask() {
//         let whatInfo = prompt("What you want to know about student?");
//         switch (whatInfo) {
//             case "name":
//             case "ім'я":
//                 console.log(this.name);
//                 break;
//             case "lastName":
//             case "прізвище":
//                 console.log(this.surname);
//                 break;
//             case "homeworks":
//             case "оцінка":
//                 console.log(this.works);
//                 break;
        
//             default:
//                 console.error("Вибачте таких даних немає");
//                 break;
//         }
//     }
// }
// danItStudent.ask()


/*
 * Завдання 9.
 *
 * Створити функцію, яка перевіряє, чи є переданий до неї рядок Паліндром (анна це паліндром).
 *
 * Підказка: потрібно перетворити рядок на масив
 *
 */
// let userName = prompt("Word");

// function reverse() {
//     return userName.split("").reverse().join("").toLowerCase() === userName.toLowerCase() ? console.log("Palindrom") : console.log("NOT palindrom");
// }
// reverse();



/*
 * Завдання 10.
 *
 * Напишіть функцію, яка очищає масив від небажаних значень,
 *
 * Таких як false, undefined, порожні рядки, нуль, null.
 *
 */

// const data = [0, 1, false, 2, undefined, '', 3, null];
// console.log(compact(data)) // [1, 2, 3]


function cleaner(arr, type) {

    return arr.filter(item => typeof item !== type)
}

let array = [0, 1, false, 2, undefined, '', 3, null];
console.log(cleaner(array, "number"));


function cleaner2(array) { return array.filter((value) => { return value !== false && value !== undefined && value !== 0 && value !== "" && value !== null }) };
console.log(cleaner2(array));
// alert("Hello world!");

//small comment

/*
Big comment
Big comment
Big comment
*/

// let i = 1;
// console.error("Hi i am error");
// console.error(i);







// strict mode!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// "use strict";
// num = 10;
// console.log(num);








// ALERT PROMPT CONFIRM

// alert("message");

// let age = prompt("Your age", 18);
// alert(`Your age ${age}`);

// confirm("Are you admin?");

// ALERT PROMPT CONFIRM







// const number = 123;
// const string = "some text";
// const string2 = "some text123";
// const booleanTrue = true;
// const booleanFalse = false;
// const array = [1, "string", booleanFalse];
// console.log(array);
// const undefinedType = undefined;
// const nan = NaN;
// const nullType = null;

// console.log(isNaN(number));
// console.log(typeof number);














// alert(Number("123"));
// alert(Number("123fa"));
// alert(Number(true));
// alert(Number(false));




// alert(Boolean(""));
// alert(Boolean(" "));
// alert(Boolean(null));
// alert(Boolean(undefined));




/**
 * ЗАВДАННЯ 1
 *
 * Створити змінну, записати до неї число, вивести значення цієї змінної в консоль.
 */

// const i = 10;
// console.log("Console log number:", i);




/**
 * ЗАВДАННЯ 2
 *
 * Створити змінну за допомогою ключового слова const, записати до неї число.
 * Присвоїти цій змінній нове число.
 */

// const i = 10;
// console.log(i);

//якщо пишемо true то він завжди виконується і приводить результат до булевого
// if (true) {
//     const i = 11;
//     console.log(i);
// }




/**
 * ЗАВДАННЯ 3
 *
 * Записати в змінну '123', вивести в консоль типуз цієї змінної.
 * Перетворити цю змінну на чисельний тип за допомогою parseInt(), parseFloat(), унарний плюс +
 * Після цього повторно вивести у консоль typeof цієї змінної.
 */

// let i = '123';
// console.log(typeof i);
// i = parseInt(i);
// console.log(typeof i);
// i = parseFloat(i);
// console.log(typeof i);
// console.log();
// i = +i;
// console.log(typeof i);










/**
 * ЗАВДАННЯ 4
 *
 * Вивести на екран повідомлення із текстом "Hello! This is alert" за допомогою модального вікна alert.
 */


// alert("Hello! This is alert");














/**
 * ЗАВДАННЯ 5
 *
 * Вивести на екран модальне вікно prompt із повідомленням "Enter the number".
 * Результат виконання модального вікна записати до змінної, значення якої вивести в консоль.
 */

// let userNumber = prompt("Enter the number");
// console.log(userNumber);
// console.log(typeof userNumber);








/**
 * ЗАВДАННЯ 6
 *
 * За допомогою модального вікна prompt отримати від користувача два числа.
 * Вивести в консоль суму, різницю, результат множення, результат поділу та залишок від поділу їх один на одного.
 */

// let numberOne = +prompt("Number one");
// let numberTwo = +prompt("Number two");

// console.log(numberOne + numberTwo);
// console.log(numberOne - numberTwo);
// console.log(numberOne * numberTwo);
// console.log(numberOne / numberTwo);
// console.log(numberOne % numberTwo);
// console.log(numberOne ** numberTwo);

/**
 * ЗАВДАННЯ 7
 *
 * Пояснити поведінку кожної операції.
 */

// const number = 123;
// const string = "hello";

// console.log(number + string);
// console.log(number - string);
// console.log(number * string);




let img = document.querySelector('img');

img.onclick = () => {
    let mySrc = img.getAttribute("src");
    if (mySrc === "./git-init.png") {
        img.setAttribute("src", "./git-init2.png");
    } else {
        img.setAttribute("src", "./git-init.png");
    }
}
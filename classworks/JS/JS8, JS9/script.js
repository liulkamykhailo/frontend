// let arr = [];

// let fruits = ["Apple", "Orange", "Plum"];
// console.log(fruits.length);
// console.log(fruits[0]);
// console.log(fruits[1]);


// let array = [1, 2, 3, 4, 5, a, b, c];
// for (let i = 0; i < array.length; i++) {
//     console.log(array[i]);
// }

// arr.concat(arr1, arr2); створює новий масив в який копіює дані

// let arrForfind = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 },];
// console.log(arrForfind.find(item => item.id === 1)); //цей метод виглядає так само як цикл for

// let arrForFilter = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 },];
// console.log(arrForFilter.filter(element => element.id > 3));

// let arr = [1,2,3,4,5,6,7];
// console.log(arr.map((item, index) => item * 10));

// let arr = [1, 2, 3, 4, 5, 6, 7];
// console.log(arr.forEach(item => item + 1)); //вертає undefined ніяк не змінює масив, перебирає його
// console.log(arr); //не змінює масив


// let arr = [1, 2, 15, 3, 11, 7, 4];
// console.log(arr.sort()); //сортує як рядки
// console.log(arr.sort((a, b) => a - b));


/**
 * Завдання 1.
 *
 * 1.Створіть масив styles з елементами “Jazz” та “Blues”.
 * 2.Додайте “Rock-n-Roll” в кінець масиву.
 * 3.Замініть значення в середині масиву на “Classics”. Ваш код повинен шукати медіанний елемент у масивах будь-якої довжини.
 * 4.Видаліть перший елемент масиву та покажіть його.
 * 5.Вставте Rap та Reggae на початок масиву.
 */

/* Вигляд масиву по ходу виконання операцій: 
Jazz, Blues
Jazz, Blues, Rock-n-Roll
Jazz, Classics, Rock-n-Roll
Classics, Rock-n-Roll
Rap, Reggae, Classics, Rock-n-Roll
*/

// let styles = ["Jazz", "Blues"];
// console.log(styles);
// styles.push("Rock-n-Roll");
// styles[2] = "Rock-n-Roll";
// console.log(styles);
// let midddleIndex = Math.floor(styles.length / 2);
// styles.splice(midddleIndex, 1, "Classics");
// console.log(styles);
// styles.splice(0, 1);
// console.log(styles.slice(0, 1));
// styles.unshift("Rap", "Reggae");
// console.log(styles);


/**
 * Завдання 2.
 *
 * Написати функцію я збільшуе єлементи массиву на 1 одиницю, через метод та через циклю
 */

// let arr = [1, 2, 3, 4, 5];
// console.log(arr.map((item) => item + 1));

// let arr1 = [10,20,30];

// for (let i = 0; i < arr1.length; i++){
//     arr1[i] = arr1[i] +1;
// }
// console.log(arr1);


/**
 * Завдання 3.
 *
 * Написати функцію яка повертає сумму всіх чисел массива
 *
 */

// const arr = [1, 2, 3, 4, 5];
// const arrSecond = [
//   1,
//   "some string",
//   2,
//   "some string",
//   3,
//   "some string",
//   4,
//   "some string",
//   5,
// ];

// let result = arr.reduce((sum, current) => sum + current, 0);
// console.log(result);

//через цикл:
// let sum = 0;
// for (let i = 0; i < arr.length; i++) {
//     sum += arr[i];
// }
// console.log(sum);


/////Урок 9//////////Урок 9//////////Урок 9//////////Урок 9//////////Урок 9//////////Урок 9//////////Урок 9//////////Урок 9//////////Урок 9//////////Урок 9//////////Урок 9//////////Урок 9/////

/**
 * Завдання 1.
 *
 * 1.Створіть масив styles з елементами “Jazz” та “Blues”.
 * 2.Додайте “Rock-n-Roll” в кінець масиву.
 * 3.Замініть значення в середині масиву на “Classics”. Ваш код повинен шукати медіанний елемент у масивах будь-якої довжини.
 * 4.Видаліть перший елемент масиву та покажіть його.
 * 5.Вставте Rap та Reggae на початок масиву.
 */

/* Вигляд масиву по ходу виконання операцій: 
Jazz, Blues
Jazz, Blues, Rock-n-Roll
Jazz, Classics, Rock-n-Roll
Classics, Rock-n-Roll
Rap, Reggae, Classics, Rock-n-Roll
*/

// let styles = ["Jazz", "Blues",];
// console.log(styles);

// styles.push("Rock-n-Roll");
// console.log(styles);

// styles[Math.floor(styles.length / 2)] = "Classics";
// console.log(styles);

// console.log(styles.shift());
// console.log(styles);

// styles.unshift("Rap", "Reggae");
// console.log(styles);


/**
 * Завдання 2.
 *
 * Написати функцію я збільшуе єлементи массиву на 1 одиницю, через метод та через цикл
 */

// let arr = [25, 49, 468, 641, 77];
// console.log(arr);

// console.log(arr.map((item) => item + 1)); //бо цей метод повертає новий масив
// console.log(arr);

// for (let i = 0; i < arr.length; i++){
//     arr[i] += 1;
// }
// console.log(arr);


/**
 * Завдання 3.
 *
 * Написати функцію яка повертає сумму всіх чисел массива
 *
 */

// const arr = [1, 2, 3, 4, 5];
// const arrSecond = [
//   10,
//   "some string",
//   20,
//   "some string",
//   30,
//   "some string",
//   40,
//   "some string",
//   50,
// ];

// let sum = 0;
// for (let i = 0; i < arr.length; i++) {
//     sum += arr[i];
// }
// console.log(sum);

// let result = arr.reduce((accumulator, item) => accumulator + item, 0);
// console.log(result);


// let arrSecondNumbers = arrSecond.filter(item => typeof item === "number");
// console.log(arrSecondNumbers);
// let result = arrSecondNumbers.reduce((accum, item) => accum + item, 0);
// console.log(result);







/**
 * Завдання 4.
 *
 * Написати функцію-помічник комірника переміщення ігор.
 *
 * Функція повинна замінювати вказаний товар новими товарами.
 *
 * Функція має два параметри:
 * - Ім'я товару, який необхідно видалити;
 * - Список товарів, якими необхідно замінити віддалений товар.
 *
 * Функція не має значення, що повертається.
 *
 * Умови:
 * - генерувати помилку, якщо ім'я товару для видалення не присутнє в масиві;
 * - Генерувати помилку, список товарів для заміни віддаленого товару не є масивом.
 *
 * Нотатки:
 * - Даний «склад товарів» у вигляді масиву з товарами через.
 */

/* Дано */
// let storage = [
//   "apple",
//   "water",
//   "banana",
//   "pineapple",
//   "tea",
//   "cheese",
//   "coffee",
// ];
// let deleteItem;
// let addedItem;
// function helper(deleteItem, addedItem) {
//     if (!Array.isArray(addedItem) || deleteItem === undefined) {
//         alert("Error");
//     }
//     storage.splice(storage.indexOf(deleteItem), 1, ...addedItem)
// }
// helper("apple", [1, 2, 3]);
// console.log(storage);



/*
 * Завдання 1.
 *
 * Створити масив об'єктів students (у кількості 7 шт).
 * У кожного студента має бути ім'я, прізвище та напрямок навчання - Full-stask чи Front-end.
 * Кожен студент повинен мати метод, sayHi, який повертає рядок `Привіт, я ${ім'я}, студент Dan IT, напрям ${напрямок}`.
 * Перебрати кожен об'єкт і викликати у кожного об'єкта метод sayHi;
 *
 */


// console.log(students[0].sayHi());

// function createStudents(userCount) {
//     let studentsArray = [];
//     for (let i = 0; i < userCount; i++) {
//         studentsArray[i] = { name: "Ivan" + i, speciality: i %2===0? "Full-stack" : "Front-end", sayHi(){console.log(`Привіт, я ${this.name}, студент Dan IT, напрям ${this.speciality}`)}}
//     }
//     return studentsArray;
// }

// let students = createStudents(7);
// console.log(students);
// students.forEach(item => item.sayHi());


/*
 * Завдання 2.
 *
 * Є масив брендів автомобілів ["bMw", "Audi", "teSLa", "toYOTA"].
 * Вам потрібно отримати новий масив, об'єктів типу
 * { type: 'car', brand: ${елемент масиву} }
 *
 * Вивести масив у консоль
 */

// let auto = ["bMw", "Audi", "teSLa", "toYOTA"];

// let autoBrand = auto.map(brand => ({ type: 'car', brand }));
// console.log(auto);
// console.log(autoBrand);

/*
 * Завдання 3.
 *
 * Створити масив чисел від 1 до 100.
 * Відфільтрувати його таким чином, щоб до нового масиву не потрапили числа менше 10 і більше 50.
 * Вивести у консоль новий масив.
 *
 */
function createArrayOfNumbers(number) {
    let arrayOfNumbers = [];
    for (let i = 0; i < number; i++) {  
        arrayOfNumbers[i] = i + 1;  
    }
    return arrayOfNumbers;
}
let number = createArrayOfNumbers(100);
console.log(number);

console.log(number.filter(item => (item >= 10 && item <= 50)));
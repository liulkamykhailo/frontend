// /*
//  * Завдання 5.
//  *
//  * Запитати в користувача ім'я, прізвище, вік (перевірити на правильну цифру
//  * і що число між 15 і 100), чи є діти, якого кольору очі.
//  * Усі ці дані записати в об'єкт.
//  * Перебрати об'єкт і вивести у консолі фразу
//  * `Властивість ${name}: ${значення name}` і так з усіма властивостями
//  */




// // let userName = prompt("Your name");
// // let userSurame = prompt("Your surname");
// let userAge = prompt("Your age");
// while (+userAge > 100 || +userAge < 15 || isNaN(userAge) || userAge === "") {
//     userAge = prompt("Your age");
// }
// // let userChildrens = prompt("Your childrens");
// // let userEyesColor = prompt("Your eyes color");

// let newUser = {
//     name: prompt("Your name"),
//     surname: prompt("Your surname"),
//     age: userAge,
//     userChildren: prompt("Your childrens"),
//     userEyesColor: prompt("Your eyes color"),
// };
// console.log(newUser);

// for (const key in newUser) {
// console.log(`Властивість ${key}: ${newUser[key]}`);
// }
// // function userAge(arr) {
// //     arr.age = prompt("Your age");
// //         while (newUser.age > 100 || +userAge < 15 || isNaN(userAge) || userAge === "") {
// //     userAge = prompt("Your age");
// // }
// // }






/**
 * Завдання 6.
 *
 * За допомогою циклу for...in вивести в консоль усі властивості
 * першого рівня об'єкта у форматі «ключ-значення».
 *
 * Просунута складність:
 * Поліпшити цикл так, щоб він умів виводити властивості об'єкта другого рівня вкладеності.
 */

/* Дано */
// const user = {
//   firstName: "Walter",
//   lastName: "White",
//   job: "Programmer",
//   pets: {
//     cat: "Kitty",
//     dog: "Doggy",
//   },
// };

// console.log(Object.keys(user));
// console.log(Object.entries(user));
// console.log(Object.values(user));



// for (const key in user) {
    
//     if (typeof user[key] == "object") {
        
//         for (const key1 in user[key]) {
//         console.log(`${key1}: ${user[key][key1]}`);
//         }
//     } else {
//         console.log(`${key} : ${user[key]}`);
//     }
// }







// /**
//  * Завдання 7.
//  *
//  * Написати функцію-помічник для ресторану.
//  *
//  * Функція має два параметри:
//  * - Розмір замовлення (small, medium, large);
//  * - Тип обіду (breakfast, lunch, dinner).
//  *
//  * Функція повертає об'єкт із двома полями:
//  * - totalPrice - загальна сума страви з урахуванням її розміру та типу;
//  * - totalCalories — кількість калорій, що міститься у блюді з урахуванням його розміру та типу.
//  *
//  * Нотатки:
//  * - Додаткові перевірки робити не потрібно;
//  * - У рішенні використовувати референтний об'єкт з цінами та калоріями.
//  */

// /* Дано */
// const priceList = {
//   sizes: {
//     small: {
//       price: 15,
//       calories: 250,
//     },
//     medium: {
//       price: 25,
//       calories: 340,
//     },
//     large: {
//       price: 35,
//       calories: 440,
//     },
//   },
//   types: {
//     breakfast: {
//       price: 4,
//       calories: 25,
//     },
//     lunch: {
//       price: 5,
//       calories: 5,
//     },
//     dinner: {
//       price: 10,
//       calories: 50,
//     },
//   },
// };

// // console.log(priceList.sizes.small.price);
// // console.log(priceList.sizes);
// // function helper(i) {
// //     let b = priceList.types[i].price
// //     let arr = {
// //         price: b,

// //     }
// //     // return console.log(priceList.sizes[type].price);
// //     return arr;
// // }
// // helper("breakfast")

// function helper(size, type) {
    
//     // let totalPrice = priceList.sizes[size].price + priceList.types[type].price;
//     // let totalCalories = priceList.sizes[size].calories + priceList.types[type].calories;
//     // console.log(totalPrice);
//     // console.log(totalCalories);

//     let arr = {
//         a: priceList.sizes[size].price,
//         b: priceList.sizes[size].calories,
//     }
//     return console.log(arr);
// }
// helper('small', 'lunch');



// /**
//  * Завдання 8.
//  *
//  * Написати функцію-фабрику, яка повертає об'єкти користувачів.
//  *
//  * Об'єкт користувача має три властивості:
//  * - Ім'я;
//  * - Прізвище;
//  * - Професія.
//  *
//  * Функція-фабрика в свою чергу має три параметри,
//  * які відбивають вищеописані властивості об'єкта.
//  * Кожен параметр функції має значення за замовчуванням: null.
//  */


// function createUser(name = null, surname = null, profession = null) {
//     let user = {
//         name: name,
//         surname: surname,
//         profession: profession,
//     }
//     return user;
// }
// console.log(createUser("a", "b"));






/*
 * Завдання 1.
 *
 * Напишіть функцію isEmpty(obj), яка повертає true, якщо об’єкт не має властивостей, інакше false.
 *
 */

// const object = {
//     asdf:"",
// };

// function isEmpty(obj) {
//     for (const key in obj) {
//         obj[key] ? console.log('false') : console.log("true");
//     }
// }
// isEmpty(object)



// RECURSION

// let count = 0;

// function invoke() {
//     console.log(`level ${count}`);
//     count = count + 1;
//     if (count <= 5) {
//         invoke();
//     }
// }

// console.log("Recursive start");
// invoke();
// console.log("Recursive finish");
// console.log(count);

// FACTORIAL

// function factorial(n) {
//     if (n === 0)
//       { return 1; }
//     else
//       { return n * factorial( n - 1 ); }
// };
// console.log(factorial(10));

/**
 * Завдання 1.
 *
 * Користувач вводить 3 числа.
 * Вивести в консоль повідомлення з максимальним числом із введених.
 *
 * Якщо одне із введених користувачем чисел не є числом,
 * вивести повідомлення: «⛔️ Помилка! Одне з введених чисел не є числом..
 *
 * Умови: об'єктом Math користуватися не можна.
 */

// let num1 = prompt("Enter number 1:");
// let num2 = prompt("Enter number 2:");
// let num3 = prompt("Enter number 3:");

// function isValidNumber(number) {
//     if (isNaN(number) || number === null || number === "") {
//         return null;
//     } else { return +number };
// }

// if (isValidNumber(num1) && isValidNumber(num2) && isValidNumber(num3)) {
//     alert(Math.max(num1, num2, num3));
// } else {
//     alert("⛔️ Помилка! Одне з введених чисел не є числом")
// }

// console.log(isValidNumber(num1));
// console.log(isValidNumber(num2));
// console.log(isValidNumber(num3));

/**
 * Завдання 1.
 *
 * Напишіть функцію sumTo(n), що обчислює суму чисел 1 + 2 + ... + n.
 *
 */

// function sumTo(number) {
//         if (number === 1) {
//             return 1;
//     }
//     return number + sumTo(number - 1);
// }
// console.log(sumTo(10));

/**
 * Завдання 2.
 *
 *  Написати функцію яка вираховує факторіал factorial(n).
 *
 * 1! = 1
 * 2! = 2 * 1 = 2
 * 3! = 3 * 2 * 1 = 6
 * 4! = 4 * 3 * 2 * 1 = 24
 * 5! = 5 * 4 * 3 * 2 * 1 = 120
 */

// function factorial(number) {
//     if (number === 1) {
//         return 1;
//     }
//     return number * factorial(number - 1);
// }

/**
 * Завдання 2.
 *
 * Напишіть програму "Кавова машина".
 *
 * Програма приймає монети та готує напої:
 * - кава за 25 монет;
 * - Капучіно за 50 монет;
 * - Чай за 10 монет.
 *
 * Щоб програма дізналася, що робити, вона повинна знати:
 * - Скільки монет користувач вніс;
 * - Який він бажає напій.
 *
 * Залежно від того, який напій вибрав користувач,
 * програма повинна обчислити здачу та вивести повідомлення в консоль:
 * «Ваш «НАЗВА НАПОЮ» готовий. Візьміть здачу: "СУМА ЗДАЧІ".".
 *
 *Якщо користувач ввів суму без здачі, вивести повідомлення:
 * «Ваш «НАЗВА НАПОЮ» готовий. Дякую за суму без здачі! :)"
 */

//  function coffeMachine(coins, drink) {

//      switch (drink) {
//         case "кава":
//             coins - 25 === 0 ? console.log("Дякую за суму без здачі") : console.log(`Здача ${coins - 25}`);
//             break;
//         case "капучіно":
//             coins - 50 === 0 ? console.log("Дякую за суму без здачі") : console.log(`Здача ${coins - 50}`);
//             break;
//         case "чай":
//             coins - 10 === 0 ? console.log("Дякую за суму без здачі") : console.log(`Здача ${coins - 10}`);
//             break;

//         default:
//             break;
//      }
// }

// coffeMachine(70, "кава");

/**
 * Завдання 3.
 *
 * Написати програму, яка опитуватиме і вітатиме користувача.
 *
 * Програма повинна дізнатися у користувача його:
 * - Ім'я;
 * - Прізвище;
 * - Рік народження.
 *
 * Якщо користувач вводить некоректні дані,
 * програма повинна повторно опитувати його доти,
 * поки дані не будуть введені коректно.
 *
 * Дані вважається введеними некоректно, якщо:
 * - Користувач не вводить у поле жодних даних;
 * - Рік народження, введений користувачем менше, ніж 1910 або більше, ніж поточний рік.
 *
 * Коли користувач введе всі необхідні дані коректно,
 * вивести в консоль повідомлення:
 * «Ласкаво просимо, Ваш рік народження РІК_НАРОДЖЕННЯ, ім'я прізвище.».
 */

let userName = prompt("Your name");
while (correctString(userName) === null) {
    userName = prompt("Your name");
}

let userSurname = prompt("Your Surname");
while (correctString(userSurname) === null) {
    userSurname = prompt("Your name");
}

let userAge = prompt("Your year");
while (correctAge(userAge) === null) {
    userAge = prompt("Your year");
}

console.log(`Ласкаво просимо, Ваш рік народження ${userAge}, ${userName.toUpperCase()} ${userSurname.toUpperCase()}`);


function correctString(string) {
  if (!isNaN(string) || string === null || string === "") {
    return null;
  } else {
      return string;
  }
}

function correctAge(age) {
    if (isNaN(age) || age === null || age === ""|| age < 1910 || age > 2023) {
    return null;
  } else {
      return age;
  }
}

let todayYear = new Date();
console.log(todayYear.getFullYear());
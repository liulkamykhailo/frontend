/**
 * Завдання 1.
 *
 * Запитати у користувача його ім'я.
 * Написати функцію, яка виводить у консоль кількість букв у імені користувача.
 * Обробити кейс, коли на початку слова можуть бути введені пробіли, потрібно обчислити довжину без їхнього обліку.
 * Якщо введено лише цифри (отримане ім'я - це число), вивести в консоль - Помилка, введіть ім'я.
 *
 */


// let userName = prompt("Your name");
// console.log(userName);
// let userLength1 = name => console.log(name.length);
// userLength1(userName)
// // function userLength(name) {
// //     return console.log(name.length);
// // }
// // userLength(userName);

/**
 * Завдання 2.
 *
 * Написати імплементацію вбудованої функції рядка repeat(times).
 *
 * Функція повинна мати два параметри:
 * - Цільовий рядок, який потрібно повторювати
 * - Кількість повторень цільового рядка.
 *
 * Функція повинна повертати перетворений рядок.
 *
 * Умови:
 * - Генерувати помилку, якщо перший параметр не є рядком, а другий не числом.
 */


// const test = "test";
// console.log(test);
// console.log(test.repeat(4));

// function repeatText(string, number) {
//     if (typeof string !== "string" || isNaN(number)) {
//         console.error("Error");
//         return;
//     };

//     let repeatString = "";
//     for (let i = 0; i < number; i++) {
//         repeatString += string;
//     }
//     return repeatString;
// }
// console.log(repeatText(test, 2));


/**
 * Завдання 3.
 *
 * Написати функцію truncate, яка «обрізає» занадто довгий рядок, додавши в кінці три крапки «...».
 *
 * Функція має два параметри:
 * - Вихідний рядок для обрізки;
 * - Допустима довжина рядка.
 *
 * Якщо рядок довший, ніж його допустима довжина — його необхідно обрізати,
 * і конкатенувати до кінця символ три крапки так,
 * щоб разом з ним довжина рядка дорівнювала максимально допустимої довжині рядка з другого параметра.
 *
 * Якщо рядки не довші, ніж її допустима довжина.
 */

// let userString = "a";
// console.log(userString.padEnd(2, '....'));
// function truncate(string, lengthString) {
//     if (string.length > lengthString) {
//         return console.log(string.slice(0, lengthString));
//     }
// }
// truncate(userString, 5);


/**
 * Завдання 4.
 *
 * Напишіть функцію, яка приймає рядок як аргумент
 * і перетворює регістр першого символу рядка з нижнього регістра у верхній.
 *
 */

// let userText = "abcdefgxzcvzxcvzxcvzxcv1";
// function toUpper(str) {
//     console.log(str[0].toUpperCase().concat(str.slice(1, str.length)));
// }
// toUpper(userText);


/**
 * Завдання 5.
 *
 * Написати функцію, яка повертає назву дня тижня (словом),
 * яка була вказана кількість днів тому.
 *
 * Функція має один параметр:
 * - Число, яке описує на скільки днів тому ми хочемо повернутися, щоб дізнатися бажаний день.
 *
 * Умови:
 * - Використання об'єкта Date обов'язково.
 *
 */

const days = {
  0: "Неділя",
  1: "Понеділок",
  2: "Вівторок",
  3: "Середа",
  4: "Четверг",
  5: "П'ятниця",
  6: "Субота",
};


function daysCount(number) {
    let dayToday = new Date();
    let daysBack = dayToday.getDate() - number;

    console.log(daysBack);
    return console.log(days[number]);
}
daysCount(1);
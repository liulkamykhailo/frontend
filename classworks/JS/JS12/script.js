
// const ul = document.querySelector('ul')
// const cloneElement = ul.children[0].cloneNode(true)
// cloneElement.innerText = 'new text'



/**
 * Завдання 1.
 *
 * Написати скрипт, який створить квадрат довільного розміру.
 *
 * Розмір квадрата в пікселях отримати інтерактивно за допомогою діалогового вікна prompt.
 *
 * Якщо користувач ввів розмір квадрата в некоректному форматі
 * Запитувати дані повторно до тих пір, поки дані не будуть введені коректно.
 *
 * Всі стилі для квадрата встановити через JavaScript за допомогою одного рядка коду.
 *
 * Тип елемента, що описує квадрат - div.
 * Задати новоствореному елементу CSS-клас .square.
 *
 * Квадрат у вигляді стилізованого елемента div необхідно
 * зробити першим та єдиним нащадком body документа.
 */

// let pixels = prompt("Введіть число", 50);
// while (isNaN(+pixels) || pixels === null || pixels === "") {
//     pixels = prompt("Введіть число", 50);
// }
// console.log(pixels);

// const square = document.createElement('div');
// square.classList.add('square');
// square.style.cssText = `
// width: ${pixels}px;
// height: ${pixels}px;
// background-color: red;`

// document.body.prepend(square)

/**
 * Завдання 2.
 *
 * Написати функцію-фабрику квадратів createSquares.
 *
 * Функція має два параметри — кількість квадратів для створення.
 *
 * Якщо користувач ввів кількість квадратів для створення в неприпустимому форматі
 * Запитувати дані повторно до тих пір, поки дані не будуть введені коректно.
 *
 * Максимальна кількість квадратів для створення – 10.
 * Якщо користувач вирішив створити більше 10 квадратів - повідомити йому про неможливість такої операції
 * і запитувати дані про кількість квадратів для створення доти, доки вони не будуть введені коректно.
 *
 * Розмір кожного квадрата в пікселях потрібно отримати інтерактивно за допомогою діалогового вікна prompt.
 *
 * Якщо користувач ввів розмір квадрата в неприпустимому форматі
 * Запитувати дані повторно до тих пір, поки дані не будуть введені коректно.
 *
 * Колір кожного квадрата необхідно запитати після введення розміру квадрата у коректному вигляді.
 *
 * Разом послідовність введення даних про квадрати виглядає так:
 * - розмір квадрата n;
 * - колір квадрата n;
 * - розмір квадрата n + 1;
 * - Колір квадрата n+1.
 * - розмір квадрата n + 2;
 * - колір квадрата n + 3;
 * - і так далі...
 *
 * Якщо не будь-якому етапі збору даних про квадрати користувач натиснув на кнопку «Скасувати»,
 * необхідно зупинити процес створення квадратів та вивести в консоль повідомлення:
 * «Операція перервана користувачем.».
 *
 * Всі стилі для кожного квадрата встановити через JavaScript за раз.
 *
 * Тип елемента, що описує кожен квадрат - div.
 * Задати новоствореним елементам CSS-класи: .square-1, .square-2, .square-3 і так далі.
 *
 * Усі квадрати необхідно зробити нащадками body документа.
 */


// function createSquares(count,) {
//     while (isNaN(+count) || count === "" || count === null) {
//         count = prompt("Введіть правильну кількість квадратів")
//     }
//     while (count > 10) {
//         count = prompt("Введіть не більше 10 квадратів")
//     }

//     let arr = new Array(Number(count)).fill(null).map((_, index) => {                        //додали  fill бо коли масив пустий то map не хоче по ньому проходитись і масив не працює
//         let size = prompt("Введіть розмір", 50);
//         while (isNaN(+size)) {
//             size = prompt("Введіть розмір коректно", 50);
//         }
//         let color = prompt("Введіть колір", "blue");
//         while (!isNaN(color)) {
//             color = prompt("Введіть колір коректно", "blue");
//         }

//         let newSquare = document.createElement('div');
//         newSquare.classList.add(`square-${index + 1}`);
//         newSquare.style.cssText = `
//         width: ${size}px;
//         height: ${size}px;
//         background-color: ${color}`;

//         return newSquare;
//     });
//     console.log(arr);

//     arr.forEach((element) => {
//         document.body.append(element)
//     })
// }
// createSquares(prompt("Введіть кількість квадратів"),)




// Те що робив Женя на уроці ||
//                           V
// function createSquares(count) {
//   while (isNaN(count) || count === null) {
//     count = prompt("Ведіть правильну кількість квадратів");
//   }
//   while (count > 10) {
//     count = prompt("Ведіть правильну кількість , менше 10 ");
//   }
//   let arr = new Array(Number(count)).fill(null).map((_, index) => {
//     let size = prompt(" Введіть розмір", 75);
//     while (isNaN(size)) {
//       size = prompt(" Введіть корректний розмір", 75);
//     }
//     let color = prompt("Введіть колір ", "red");
//     while (!isNaN(color)) {
//       color = prompt(" Введіть корректний колір", "red");
//     }

//     let newSqueare = document.createElement("div");
//     newSqueare.classList.add(`square-${index + 1}`);
//     newSqueare.style.cssText = `
//     background-color: ${color};
//     height: ${size}px;
//     width: ${size}px;

//     `;

//     return newSqueare;
//   });
//   console.log(arr);

//   arr.forEach((element) => {
//     document.body.append(element);
//   });
// }

// createSquares(prompt("Ведіть кількість квадратів"));





/**
 * Завдання 4.
 *
 * Написати тудушку.
 *
 * Запитувати у користувача пункти для додавання до списку доти, доки не натисне скасування.
 * Кожен prompt є новий елемент списку.
 * Усі пункти вставити на сторінку.
 *
 * Не забуваємо про семантику (список має бути оформлений через ul або ol);
 *
 * При натисканні на елемент списку - видалити цей пункт.
 *
 * Підказка
 * elem.onclick = function() {
 * пишемо що відбувається на клік
 * };
 */



let li = prompt("Enter ...");

let arr = [];
while (li !== null) {
    li = prompt("Enter ...");
    let liElement = document.createElement('li');
    liElement.innerText = li;
    liElement.onclick = function() {
console.log(liElement.remove());
};
    arr.push(liElement)
}
console.log(arr);



let ulElement = document.createElement('ul');
document.body.append(ulElement)
let ul = document.querySelector('ul')
arr.forEach(element => {
    ul.append(element)
})



// let ul = document.createElement('ul');
// let li = document.createElement('li');
// while () {
//     let text = prompt("Enter ...");
//     li.innerHTML = `<strong>${text}</strong> Ви прочитали важливе повідомлення.`;
//     ul.append(li)
    
// }
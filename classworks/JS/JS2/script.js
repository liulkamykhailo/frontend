// OR

// console.log("0" || false);
// console.log(null || false);


// const hour = 19;
// if (hour <9 || hour>18) {
// console.log("Office is closed");
// }



// console.log(null || 1);//1
// console.log(null || 0 || 1);//1
// console.log(undefined || null || 0);//0
// console.log(undefined || "0" || null);//0





//AND

// console.log(true && true);
// console.log(true && false);
// console.log(null && 1);//null
// console.log(null && 0 && 1);//null
// console.log(undefined && null && 0);//undefined
// console.log("0" && undefined && null);//undefined


// let hour = 12;
// let minutes = 30;

// if (hour == 12 && minutes == 30) {
//     console.log("Now 12:30");
// }





//IF ELSE

// const a = 1;

// if (a === 1) {
//     console.log("This is 1");
// }

// if (a === 0) {
//     console.log(`a is ${a}!`);
// } else if (a === 1) {
//     console.log(`a is ${a}!`);
// } else if (a === 2) {
//     console.log(`a is ${a}!`);
// } else {
//     console.log("last else");
// }




//SWITCH

// let i = 10;

// switch (i) {
//     case 0:
//         i += 1;
//         break;
//     case 1:
//         i += 1;
//         break;
//     case 2:
//         i += 1;
//         break;
//     case 3:
//         i += 1;
//         break;
//     case 4:
//         i += 1;
//         break;
//     default:
//         i += 10;
//         break;
// }
// console.log(i);
// console.log("2", i);





//FOR

// for (let i = 0; i <= 10; i++) {
//     console.log(i);
// }





//while
// let i = 1;
// // while (i <=10) {
// //     console.log(i);
// //     i++;
// // }


// do {
//     console.log(i);
//     i++;
// } while (i <= 10);













/**
 * Завдання 1.
 *
 * Що виведеться у консолі?
 *
 */
// console.log("1 =>", null || 2 || undefined);
// console.log("2 =>", alert(1) || 2 || alert(3));
// console.log("3 =>", 1 && null && 2);
// console.log("4 =>", alert(1) && alert(2));
// console.log("5 =>", null || (2 && 3) || 4);


/**
 * Завдання 1.
 *
 * Користувач вводить у модальне вікно будь-яке число.
 *
 * У консоль вивести повідомлення:
 * - Якщо число парне, вивести в консоль повідомлення "Ваше число парне."";
 * - Якщо число не парне, вивести в консоль повідомлення "Ваше число не парне."";
 * - Якщо користувач ввів не число, вивести нове модальне вікно з повідомленням "Необхідно ввести число!".
 * - Якщо користувач вдруге запровадив не число, вивести повідомлення: «⛔️ Помилка! Ви ввели не число..
 */

// "use strict"

// let numberUser = +prompt("Enter your number");
// console.log(numberUser, typeof numberUser);
// console.log(isNaN(numberUser));
// if (!Number(numberUser) || isNaN(numberUser)) {
//     console.log("Необхідно ввести число!");
//     numberUser = +prompt("Enter your number");
// };
// if ((numberUser % 2) === 0) {
//     console.log("Even");
// } else{
//     console.log("Odd");
// }

/**
 * Завдання 2.
 *
 * Написати програму, яка вітатиме користувача.
 * Спочатку користувач вводить своє ім'я, після чого програма виводить у консоль повідомлення з урахуванням його посади..
 *
 * Список посад:
 * Mike — CEO;
 * Jane — CTO;
 * Walter — програміст:
 * Oliver — менеджер;
 * John — уборщик.
 *
 * Якщо введено невідоме програмі ім'я, вивести в консоль повідомлення «Користувача не знайдено»..
 *
 * Виконати завдання у двох варіантах:
 * - використовуючи конструкцію if/else if/else;
 * - використовуючи конструкцію switch.
 */


// const userName = prompt("your namE").toLowerCase();
// console.log(userName);
// // userName.toLowerCase();
// console.log(userName);

// switch (userName) {
//     case "i":
//         console.log("asdf");
//         break;

//     default:
//         break;
// }




/**
 * Завдання 3.
 *
 * Користувач вводить 3 числа.
 * Вивести в консоль повідомлення з максимальною кількістю з введених.
 *
 * Якщо одне із введених користувачем чисел не є числом,
 * вивести повідомлення: «⛔️ Помилка! Одне з введених чисел не є числом..
 *
 * Умови: об'єктом Math користуватися не можна.
 */

/**
 * Завдання 4.
 *
 * Напишіть програму "Кавова машина".
 *
 * Програма приймає монети та готує напої:
 * - кава за 25 монет;
 * - Капучіно за 50 монет;
 * - Чай за 10 монет.
 *
 * Щоб програма дізналася, що робити, вона повинна знати:
 * - Скільки монет користувач вніс;
 * - Який він бажає напій.
 *
 * Залежно від того, який напій вибрав користувач,
 * програма повинна обчислити здачу та вивести повідомлення в консоль:
 * «Ваш «НАЗВА НАПОЮ» готовий. Візьміть здачу: "СУМА ЗДАЧІ".".
 *
 *Якщо користувач ввів суму без здачі, вивести повідомлення:
 * «Ваш «НАЗВА НАПОЮ» готовий. Дякую за суму без здачі! :)"
 */





 /**
 * Завдання 1.
 *
 * За допомогою циклу вивести в консоль всі непарні числа,
 * які перебувають у діапазоні від 0 до 300.
 *
 * Примітка:
 * парне число - це число, яке ділиться на два.
 * Непарне число - це число, яке не ділиться на два.
 *
 * Просунута складність:
 * Не виводити в консоль числа, які діляться на 5.
 */


// for (let i = 0; i <= 300; i++){
//     // console.log(i);
//     if (i % 2 !== 0) {
//         console.log(i);
//     }
// };


/**
 * Завдання 2.
 *
 * Користувач повинен ввести два числа.
 * Якщо введене значення не є числом,
 * програма продовжує опитувати користувача до того часу,
 * Поки він не введе число.
 *
 * Коли користувач введе два числа, вивести в консоль повідомлення:
 * «Вітаємо. Введені вами числа: «ПЕРШЕ_ЧИСЛО» і «ДРУГЕ_ЧИСЛО».».
 */


// let userNumber = prompt("Your number 1");
// console.log(userNumber);
// while (Number.isNaN(Number(userNumber)) || userNumber === null) {
//     userNumber = prompt("Your number 1");
// }
// let userNumber2 = prompt("Your number 2");
// while (Number.isNaN(Number(userNumber2)) || userNumber2 === null) {
//     userNumber2 = prompt("Your number 1");
// }
// console.log(`Your num 1 ${userNumber}, your num 2 ${userNumber2}`);
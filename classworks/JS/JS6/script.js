/*
 * Завдання 1.
 *
 * Напишіть функцію isEmpty(obj), яка повертає true, якщо об’єкт не має властивостей, інакше false.
 *
 */

// let user1 = {

// };
// let user2 = {
//     name: "Mykhailo",
//     lastName: "Liulka",
// }

// function isEmpty(obj) {
//     for (let key in obj) {
//         return false;
//     }
//     return true;
// };
// console.log(isEmpty(user1));
// console.log(isEmpty(user2));


/**
 * Завдання 2.
 *
 * У нас є об’єкт для зберігання заробітної плати нашої команди:
 *
 * Напишіть код для підсумовування всіх зарплат і збережіть у змінній sum. У наведеному вище прикладі має бути 790.
 *
 */

// let salaries = {
//   Anna: 200,
//   Oleksandr: 100,
//     Julia: 100,

// };


// function salary(obj) {
//     let sum = 0;
//     for (const key in obj) {
//         sum += obj[key];
//     }
//     // console.log(sum);
//     return console.log(sum);
// }
// console.log(salary(salaries)); 

/**
 * Завдання 3.
 *
 * Створіть функцію multiplyNumeric(obj), яка примножує всі числові властивості об’єкта obj на 2.
 *
 */

// let menu = {
//   width: 200,
//   height: 300,
//   title: "Моє меню",
// };

// function multiplyNumeric(obj) {
//     const newObj = {...obj };
//     for (let key in newObj) {
//         if (typeof newObj[key] === 'number') {
//             newObj[key] *= 2;
//         }
//     }
//     return newObj;
// };
// console.log(multiplyNumeric(menu));

/**
 * Завдання 4.
 *
 * За допомогою циклу for...in вивести в консоль усі властивості
 * першого рівня об'єкта у форматі «ключ-значення».
 *
 * Просунута складність:
 * Поліпшити цикл так, щоб він умів виводити властивості об'єкта другого рівня вкладеності.
 */

/* Дано */
// const user = {
//   firstName: "Walter",
//   lastName: "White",
//   job: "Programmer",
//   pets: {
//     cat: "Kitty",
//     dog: "Doggy",
//   },
// };


// function key(obj) {
//     for (const key in obj) {
//         console.log(`Key ==> ${key} |||| Value ==> ${obj[key]}`);
//         if (typeof obj[key] === 'object') {
//             for (const key1 in obj[key]) {
//                 console.log(`Key ==> ${key} |||| Value ==> ${obj[key][key1]}`);
//             }
//         } else {
//             console.log(`Key ==> ${key} |||| Value ==> ${obj[key]}`);
//         }
//     }
// }
// key(user);

/**
 * Завдання 5.
 *
 * Написати функцію-помічник для ресторану.
 *
 * Функція має два параметри:
 * - Розмір замовлення (small, medium, large);
 * - Тип обіду (breakfast, lunch, dinner).
 *
 * Функція повертає об'єкт із двома полями:
 * - totalPrice - загальна сума страви з урахуванням її розміру та типу;
 * - totalCalories — кількість калорій, що міститься у блюді з урахуванням його розміру та типу.
 *
 * Нотатки:
 * - Додаткові перевірки робити не потрібно;
 * - У рішенні використовувати референтний об'єкт з цінами та калоріями.
 */

/* Дано */
// const priceList = {
//   sizes: {
//     small: {
//       price: 15,
//       calories: 250,
//     },
//     medium: {
//       price: 25,
//       calories: 340,
//     },
//     large: {
//       price: 35,
//       calories: 440,
//     },
//   },
//   types: {
//     breakfast: {
//       price: 4,
//       calories: 25,
//     },
//     lunch: {
//       price: 5,
//       calories: 5,
//     },
//     dinner: {
//       price: 10,
//       calories: 50,
//     },
//   },
// };

// function restourant(size, type) {
//     let totalPrice = 0;
//     let totalCalories = 0;
//     totalPrice = priceList.sizes[size].price + priceList.types[type].price
//     totalCalories = priceList.sizes[size].calories + priceList.types[type].calories
//     console.log(totalPrice);
//     console.log(totalCalories);
//     return new Object(totalPrice, totalCalories);
// }
// console.log(restourant("large", "lunch"));


/**
 * Завдання 6.
 *
 * Написати функцію-фабрику, яка повертає об'єкти користувачів.
 *
 * Об'єкт користувача має три властивості:
 * - Ім'я;
 * - Прізвище;
 * - Професія.
 *
 * Функція-фабрика в свою чергу має три параметри,
 * які відбивають вищеописані властивості об'єкта.
 * Кожен параметр функції має значення за замовчуванням: null.
 */

// function createUser(name=null, surName=null, profession=null) {
//     return {
//         name,
//         surName,
//         profession,
//     };
// }
// console.log(createUser("asdf","asdf"));


/**
 * Завдання 7.
 *
 * Створіть об’єкт calculator з трьома методами:
 *
 * read() запитує два значення та зберігає їх як властивості об’єкта з іменами firstNumber та secondNumber відповідно.
 * sum() повертає суму збережених значень.
 * mul() множить збережені значення і повертає результат.
 * divide() ділить збережені значення і повертає результат.
 */

// const sum = {
//     firstNumber : 0,
//     secondNumber : 0,
//     read() {
//         let number1 = +prompt("firstNumber");
//         let number2 = +prompt("firstNumber");
//         this.firstNumber = number1;
//         this.secondNumber = number2;
//         return this;
//     },
//     sum() {
//         console.log(this.firstNumber + this.secondNumber);
//         return this;
//     },
//     mul() {
//         console.log(this.firstNumber * this.secondNumber);
//         return this;
//     },
//     divide() {
//         console.log(this.firstNumber / this.secondNumber);
//         return this;
//     },
// }
// console.log(sum);
// sum.read().sum().mul().divide()
// // sum.sum()
// // sum.mul()
// // sum.divide()


/*
 * Завдання 0.
 *
 * Створити об'єкт користувача, який має три властивості:
 * - Ім'я;
 * - Прізвище;
 * - Професія.
 *
 * Умови:
 * - Всі властивості мають бути доступні тільки для запису;
 * - Розв'язати задачу двома способами.
 *
 */